import 'babel-polyfill';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from 'store';
import LanguageProvider from 'containers/LanguageProvider';
import { translationMessages } from 'i18n';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-material-design/dist/css/bootstrap-material-design.min.css';
import 'bootstrap-material-design-icons/css/material-icons.min.css';
import 'react-select/dist/react-select.css';

import MainRouter from './routing';

const store = configureStore(browserHistory);
const syncedHistory = syncHistoryWithStore(browserHistory, store);

const renderApp = messages => (
  render(
    <Provider store={store}>
      <LanguageProvider messages={messages}>
        <MainRouter
          onUpdate={() => window.scrollTo(0, 0)}
          history={syncedHistory}
          store={store}
        />
      </LanguageProvider>
    </Provider>,
    document.getElementById('root'),
  )
);


// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  (new Promise((resolve) => {
    resolve(import('intl'));
  }))
  .then(() => Promise.all([
    import('intl/locale-data/jsonp/en.js'),
  ]))
  .then(() => renderApp(translationMessages))
  .catch((err) => {
    throw err;
  });
} else {
  renderApp(translationMessages);
}
