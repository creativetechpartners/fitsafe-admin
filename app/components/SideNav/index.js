import React from 'react';
import { array, func, string } from 'prop-types';
import './styles.scss';

const SideNav = ({ onSelect, surveys, onEdit, onDelete, activeSurveyId }) => (
  <div className="side-nav">
    {surveys.map(survey => (
      <div
        onClick={() => onSelect(survey)}
        className="category-container"
        key={survey._id}
      >
        <div
          className={`category-item ${survey._id === activeSurveyId ? 'active' : ''}`}
          role="button"
        >
          <div className="survey-name">{survey.name}</div>
          <div className="item-controls">
            <i className="material-icons" role="button" onClick={() => onEdit(survey)}>mode_edit</i>
            <i className="material-icons" role="button" onClick={() => onDelete(survey)}>delete</i>
          </div>
        </div>
      </div>
    ))}
  </div>
);

SideNav.defaultProps = {
  activeSurveyId: null,
};

SideNav.propTypes = {
  surveys: array.isRequired,
  onEdit: func.isRequired,
  onDelete: func.isRequired,
  onSelect: func.isRequired,
  activeSurveyId: string,
};

export default SideNav;
