import React from 'react';
import { func } from 'prop-types';
import './styles.scss';

const CreateFormButton = ({ clickHandler }) => (
  <div
    className="create-form-button"
    role="button"
    onClick={clickHandler}
  >
    <i className="material-icons">add</i>
  </div>
);

CreateFormButton.propTypes = {
  clickHandler: func.isRequired,
};

export default CreateFormButton;
