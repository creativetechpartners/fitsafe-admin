import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { bool, func } from 'prop-types';

const ConfirmDelete = ({ show, onHide, onDelete }) => (
  <Modal show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>
        Please confirm deleting
      </Modal.Title>
    </Modal.Header>
    <Modal.Body bsClass="modal-body clearfix">
      <Button onClick={onHide} bsStyle="default" className="btn-raised pull-left">
        Close
      </Button>
      <Button onClick={onDelete} bsStyle="danger" className="btn-raised pull-right">
        Delete
      </Button>
    </Modal.Body>
  </Modal>
);

ConfirmDelete.propTypes = {
  show: bool.isRequired,
  onHide: func.isRequired,
  onDelete: func.isRequired,
};

export default ConfirmDelete;
