import React from 'react';
import { Panel } from 'react-bootstrap';
import { object } from 'prop-types';
import './style.scss';

const SurveyDetails = ({ survey }) => {
  if (!survey) {
    return (
      <div className="survey-details">
        <h4 className="info">Click on survey in side bar</h4>
      </div>
    );
  }

  const { questions, name, surveyQuestion } = survey;
  return (
    <div className="survey-details">
      <h1 className="survey-name">{name}</h1>
      <h3>{surveyQuestion.title}</h3>
      {Object.values(questions).map(({ title, type, _id }) => (
        <Panel key={_id} className="question-preview">
          <span className="title">{title}</span>
          <span className="type pull-right">{type}</span>
        </Panel>
      ))}
    </div>
  );
};

SurveyDetails.defaultProps = {
  survey: null,
};

SurveyDetails.propTypes = {
  survey: object,
};

export default SurveyDetails;
