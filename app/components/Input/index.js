import React from 'react';
import { object, string } from 'prop-types';
import { FormControl } from 'react-bootstrap';
import './style.scss';

const Input = (props) => {
  const { input, meta: { touched }, error, ...restAtr } = props;
  return (
    <div className="input-wrapper">
      <FormControl {...input} {...restAtr} autoComplete="off" />
      {touched && error && <span className="input-error">{error}</span>}
    </div>
  );
};

Input.defaultProps = {
  error: undefined,
};

Input.propTypes = {
  meta: object.isRequired,
  input: object.isRequired,
  error: string,
};

export default Input;
