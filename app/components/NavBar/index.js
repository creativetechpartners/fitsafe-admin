import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { logOut } from 'containers/Login/actions';
import { func } from 'prop-types';
import './styles.scss';

const NavbarComponent = ({ goTo, goOut }) => (
  <div className="top-navbar">
    <a className="brand-logo" role="button" onClick={() => goTo('/')}>
      FitSafe
    </a>
    <div>Admin</div>
    <a className="log-out" role="button" onClick={() => goOut()}>
      Log out
    </a>
  </div>
);

NavbarComponent.propTypes = {
  goTo: func.isRequired,
  goOut: func.isRequired,
};

export default connect(
  null,
  dispatch => bindActionCreators({
    goTo: push,
    goOut: logOut,
  }, dispatch),
)(NavbarComponent);
