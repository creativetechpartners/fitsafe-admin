import React, { Component } from 'react';
import { Button, FormControl } from 'react-bootstrap';
import Question from 'containers/Question';
import { object } from 'prop-types';
import './styles.scss';

class CreateEditForm extends Component {
  state = this.props.survey || {
    questions: [null],
    name: '',
  };

  questions = [];
  surveySignificant = null;

  addQuestion = () => {
    this.setState({
      questions: [...this.value.questions, null],
    });
  };

  handleChangeCategory = ({ value }) => this.setState({ category: value });

  changeField = (e, field) => this.setState({ [field]: e.target.value });

  handleDelete = (questionIndex) => {
    const questions = this.value.questions.filter((el, i) => questionIndex !== i);
    this.setState({ questions });
  };

  get value() {
    const { name } = this.state;
    const questions = this.questions.filter(el => !!el).map(q => q.value);
    this.setState({ questions });

    return {
      name,
      questions,
      surveyQuestion: this.mainQuestion.value,
      significant: this.surveySignificant.checked,
    };
  }

  handleChangeSignificance = () => {};

  render() {
    const { name, surveyQuestion, questions, significant } = this.state;

    return (
      <div className="create-edit-container">
        <div>
          <div className="header-container">
            Title:
            <FormControl
              onChange={e => this.changeField(e, 'name')}
              value={name}
              name="Title"
              type="text"
              placeholder="Survey title"
            />
            Main Question:
            <Question
              data={surveyQuestion || { type: 'yes-no', title: '' }}
              ref={(ref) => {
                this.mainQuestion = ref;
              }}
              main
            />
            <div className="checkbox-wrapper">
              <input
                type="checkbox"
                ref={(ref) => {
                  this.surveySignificant = ref;
                }}
                defaultChecked={significant}
                className="checkbox"
              />
              <label>Is survey significant?</label>
            </div>
          </div>

          <div>
            Survey Questions
            {questions.map((question, i) => (
              <Question
                key={Math.random()}
                data={question}
                ref={(c) => {
                  this.questions[i] = c;
                }}
                handleDelete={() => this.handleDelete(i)}
                surveyQuestions={questions}
              />
            ))}
          </div>
        </div>
        <div className="controls">
          <Button bsStyle="primary" onClick={this.addQuestion}>
            Add question
          </Button>
        </div>
      </div>
    );
  }
}

CreateEditForm.defaultProps = {
  survey: undefined,
};

CreateEditForm.propTypes = {
  survey: object,
};

export default CreateEditForm;
