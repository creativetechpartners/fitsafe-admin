import React, { Component } from 'react';
import { Button, Row } from 'react-bootstrap';
import { object } from 'prop-types';
import _ from 'lodash';
import './styles.scss';

class Checkbox extends Component {
  state = this.props.options || {
    variants: ['', '', ''],
    variantsScores: [],
    suggestions: [],
  };

  variants = [];
  variantsScores = [];
  suggestions = [];

  addInput = () => {
    this.setState({ variants: [...this.state.variants, ''] });
  };

  get value() {
    return {
      variants: this.variants.map(({ value }) => value),
      variantsScores: this.variantsScores.map(({ value }) => value),
      suggestions: this.suggestions.map(({ value }) => value),
      attachmentsDisabled: _.get(this, 'attachmentsDisabled.checked'),
    };
  }

  render() {
    const { variants, variantsScores = [], suggestions, attachmentsDisabled = false } = this.state;

    return (
      <div className="checkbox-question-variants">
        variants:
        {variants.map((value, i) => (
          <Row className="variant-container" key={i}>
            <input
              className="col-md-9"
              ref={(c) => {
                this.variants[i] = c;
              }}
              defaultValue={value}
              placeholder="variant title"
            />
            <input
              className="col-md-3"
              type="number"
              ref={(c) => {
                this.variantsScores[i] = c;
              }}
              defaultValue={variantsScores[i]}
              placeholder="variant score"
            />
            <input
              className="col-md-12"
              type="text"
              ref={(c) => {
                this.suggestions[i] = c;
              }}
              defaultValue={suggestions[i]}
              placeholder="suggestion"
            />
          </Row>
        ))}
        <div className="checkbox-wrapper">
          <input
            type="checkbox"
            ref={(ref) => {
              this.attachmentsDisabled = ref;
            }}
            defaultChecked={attachmentsDisabled}
            className="checkbox"
          />
          <label>disable attachments</label>
        </div>
        <Button onClick={this.addInput}>Add variant</Button>
      </div>
    );
  }
}

Checkbox.defaultProps = {
  options: undefined,
};

Checkbox.propTypes = {
  options: object,
};

export default Checkbox;
