import React, { Component } from 'react';
import { object, bool } from 'prop-types';
import _ from 'lodash';
import './styles.scss';

class YesNo extends Component {
  state = this.props.options || {
    variantsScores: [],
  };

  get value() {
    const { main } = this.props;
    return {
      variants: ['yes', 'no'],
      variantsScores: main ? [] : [this.yesValue.value, this.noValue.value],
      suggestions: main ? [] : [this.suggestion.value],
      attachmentsDisabled: _.get(this, 'attachmentsDisabled.checked'),
      disabledCameraOnNo: _.get(this, 'disabledCameraOnNo.checked'),
    };
  }

  render() {
    const {
      variantsScores = [],
      suggestions = [],
      attachmentsDisabled = false,
      disabledCameraOnNo = false,
    } = this.state;
    const { main } = this.props;

    return (
      <div>
        {!main && (
          <div className="yes-no-question">
            <div>
              <input
                ref={(ref) => {
                  this.yesValue = ref;
                }}
                defaultValue={variantsScores[0]}
                placeholder="yes score value"
                type="number"
              />
            </div>
            <div>
              <input
                ref={(ref) => {
                  this.noValue = ref;
                }}
                defaultValue={variantsScores[1]}
                placeholder="no score value"
                type="number"
              />
            </div>
            <div>
              <input
                ref={(ref) => {
                  this.suggestion = ref;
                }}
                defaultValue={suggestions[0] || ''}
                placeholder="suggestion value"
                type="text"
              />
            </div>
            <div className="checkbox-wrapper">
              <input
                type="checkbox"
                ref={(ref) => {
                  this.attachmentsDisabled = ref;
                }}
                defaultChecked={attachmentsDisabled}
                className="checkbox"
              />
              <label>disable attachments</label>
            </div>
            <div className="checkbox-wrapper">
              <input
                type="checkbox"
                ref={(ref) => {
                  this.disabledCameraOnNo = ref;
                }}
                defaultChecked={disabledCameraOnNo}
                className="checkbox"
              />
              <label>Disable Camera on {'"No"'} answer</label>
            </div>
          </div>
        )}
      </div>
    );
  }
}

YesNo.defaultProps = {
  options: undefined,
};

YesNo.propTypes = {
  options: object,
  main: bool.isRequired,
};

export default YesNo;
