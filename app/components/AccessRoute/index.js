import { Route } from 'react-router';
import { push } from 'react-router-redux';

export default class AccessRoute extends Route {
  static defaultProps = {
    onEnter(nextState, replaceState, callback) {
      const { redirect, check, store: { dispatch } } = this;
      if (check()) {
        callback();
      } else {
        dispatch(push(redirect));
      }
    },
  }
}
