import React from 'react';
import { object, func } from 'prop-types';
import './styles.scss';

const CategoryCard = (props) => {
  const {
    category,
    category: { name, description, _id },
    clickHandler,
    openUpdate,
    deleteCategory,
  } = props;
  return (
    <div className="category-card" onClick={clickHandler} role="button">
      <div>
        <i className="material-icons update-icon" onClick={e => deleteCategory(e, _id)}>close</i>
        <i className="material-icons remove-icon" onClick={e => openUpdate(e, category)} >create</i>
      </div>
      <div className="title">
        <p>{name}</p>
      </div>
      <div className="description">
        <p>{description}</p>
      </div>
    </div>
  );
};

CategoryCard.propTypes = {
  category: object.isRequired,
  clickHandler: func.isRequired,
  openUpdate: func.isRequired,
  deleteCategory: func.isRequired,
};

export default CategoryCard;
