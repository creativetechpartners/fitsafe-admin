import React, { Component } from 'react';
import { object, func, bool, array } from 'prop-types';
import { FormControl, Button, Modal } from 'react-bootstrap';

const defaultCategory = {
  name: '',
  description: '',
};

class CategoryForm extends Component {
  state = {
    form: defaultCategory,
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.category !== nextProps.category) {
      this.setState({ form: nextProps.category || defaultCategory });
    }
  }

  onSubmit = () => {
    this.props.onSubmit(this.state.form);
    this.clearFields();
  }

  onUpdate = () => {
    this.props.onUpdate(this.state.form, this.props.category._id);
  }

  changeField = (e, field) => {
    this.setState({
      form: {
        ...this.state.form,
        [field]: e.target.value,
      },
    });
  }

  clearFields = () => {
    this.setState({
      form: defaultCategory,
    });
  }

  render() {
    const { show, onHide, category, categoryValidationErrors } = this.props;
    const { name, description } = this.state.form;
    return (
      <Modal show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title>
            {category ? `Update category ${category.name}` : 'Create new category' }
          </Modal.Title>
        </Modal.Header>
        <Modal.Body bsClass="modal-body clearfix">
          <form>
            <FormControl
              onChange={e => this.changeField(e, 'name')}
              value={name}
              name="name"
              type="text"
              placeholder="Category Name"
              autoFocus
            />
            <FormControl
              onChange={e => this.changeField(e, 'description')}
              value={description}
              name="description"
              type="text"
              placeholder="Description"
              autoFocus
            />
            {categoryValidationErrors.length ?
              <span className="error">
                {categoryValidationErrors[0]}
              </span> : null}
            <Button
              onClick={!category ? this.onSubmit : this.onUpdate}
              bsStyle="info"
              className="btn-raised pull-right"
            >
              {category ? 'Update' : 'Add'}
            </Button>
          </form>
        </Modal.Body>
      </Modal>
    );
  }
}

CategoryForm.defaultProps = {
  category: null,
};

CategoryForm.propTypes = {
  category: object,
  onSubmit: func.isRequired,
  onUpdate: func.isRequired,
  show: bool.isRequired,
  onHide: func.isRequired,
  categoryValidationErrors: array.isRequired,
};

export default CategoryForm;
