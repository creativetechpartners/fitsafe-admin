import React from 'react';
import Login from 'containers/Login';
import Layout from 'containers/Layout';
import Categories from 'containers/Categories';
import Surveys from 'containers/Surveys';
import AccessRoute from 'components/AccessRoute';
import NotFound from 'containers/NotFound';
import { Router, Route } from 'react-router';
import { object } from 'prop-types';

const check = ({ login: { token } }) => !!token;

const MainRouter = props => (
  <Router {...props}>
    <AccessRoute store={props.store} check={() => !check(props.store.getState())} redirect="/">
      <Route path="/login" component={Login} />
    </AccessRoute>
    <AccessRoute store={props.store} check={() => check(props.store.getState())} redirect="/login">
      <Route component={Layout}>
        <Route path="/" component={Categories} />
        <Route path="categories/:categoryId" component={Surveys} />
      </Route>
    </AccessRoute>
    <Route path="*" component={NotFound} />
  </Router>
);

MainRouter.propTypes = {
  store: object.isRequired,
};

export default MainRouter;
