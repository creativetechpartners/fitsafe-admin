import serialize from './serialize';

let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwic2VsZWN0ZWQiOnt9LCJnZXR0ZXJzIjp7fSwiX2lkIjoiNTk2ZmEyNzU3NGJmNzFiZGVmMjFiMWIxIiwid2FzUG9wdWxhdGVkIjpmYWxzZSwiYWN0aXZlUGF0aHMiOnsicGF0aHMiOnsiX192IjoiaW5pdCIsInN0YXR1cyI6ImluaXQiLCJwYXNzd29yZCI6ImluaXQiLCJlbWFpbCI6ImluaXQiLCJsYXN0TmFtZSI6ImluaXQiLCJmaXJzdE5hbWUiOiJpbml0IiwiX2lkIjoiaW5pdCJ9LCJzdGF0ZXMiOnsiaWdub3JlIjp7fSwiZGVmYXVsdCI6e30sImluaXQiOnsiX192Ijp0cnVlLCJzdGF0dXMiOnRydWUsInBhc3N3b3JkIjp0cnVlLCJlbWFpbCI6dHJ1ZSwibGFzdE5hbWUiOnRydWUsImZpcnN0TmFtZSI6dHJ1ZSwiX2lkIjp0cnVlfSwibW9kaWZ5Ijp7fSwicmVxdWlyZSI6e319LCJzdGF0ZU5hbWVzIjpbInJlcXVpcmUiLCJtb2RpZnkiLCJpbml0IiwiZGVmYXVsdCIsImlnbm9yZSJdfSwicGF0aHNUb1Njb3BlcyI6e30sImVtaXR0ZXIiOnsiZG9tYWluIjpudWxsLCJfZXZlbnRzIjp7fSwiX2V2ZW50c0NvdW50IjowLCJfbWF4TGlzdGVuZXJzIjowfX0sImlzTmV3IjpmYWxzZSwiX2RvYyI6eyJfX3YiOjAsInN0YXR1cyI6Ik5PVF9BQ1RJVkUiLCJwYXNzd29yZCI6IiQyYSQxMCRvQ0p5b3Vmd3BhckxDbVhkOFNNWkN1dnpOSFp3Q3Vrd0pJdzloM2lidnVzTjRGOEZqYmJMSyIsImVtYWlsIjoia3pnZGJvOEBkaXNwb3N0YWJsZS5jb20iLCJsYXN0TmFtZSI6ImRmdmZkdiIsImZpcnN0TmFtZSI6InZkZnN2c2RmIiwiX2lkIjoiNTk2ZmEyNzU3NGJmNzFiZGVmMjFiMWIxIn0sIiRpbml0Ijp0cnVlLCJpYXQiOjE1MDA0ODg0NzN9.7_5zU5dAhNrrB4-a9jRuAakgW9MkcWJIu6rKBa4Qhes';

export function setToken(authToken) {
  token = authToken;
}

export function fetchClient(url, params) {
  return fetch(url, params)
    .then((response) => {
      if (response.status !== 200) {
        throw response;
      } else {
        return response.json();
      }
    });
}

function callApi(url, params) {
  let preparedUrl = url;
  const preparedParams = { ...params };

  if (typeof params.query === 'object') {
    preparedUrl += `?${serialize(params.query)}`;
  } else if (typeof params.query === 'string') {
    preparedUrl += `?${preparedParams.query}`;
  }

  preparedParams.headers = {
    ...(preparedParams.headers || {}),
  };

  if (preparedParams.body && typeof preparedParams.body !== 'string') {
    preparedParams.body = JSON.stringify(preparedParams.body);
    preparedParams.headers = {
      'content-type': 'application/json',
      ...preparedParams.headers,
    };
  }

  if (token) {
    preparedParams.headers.Authorization = `Bearer ${token}`;
  }

  return fetchClient(
    `/api/${preparedUrl}`,
    preparedParams,
  );
}

export default {
  get: (url, options = {}) => callApi(url, { ...options, method: 'GET' }),
  post: (url, options = {}) => callApi(url, { ...options, method: 'POST' }),
  put: (url, options = {}) => callApi(url, { ...options, method: 'PUT' }),
  delete: (url, options = {}) => callApi(url, { ...options, method: 'DELETE' }),
};
