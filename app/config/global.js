export default {
  admin: {
    email: 'admin@mail.com',
    password: 'admin',
  },
  hotLoader: {
    host: 'localhost',
    port: 3000,
  },
  apiHost: 'http://fitsafe-admin.ctpusdev.com',
  host: 'localhost',
  port: 3001,
  formTypes: {
    'yes-no': 'Yes no question',
    radio: 'Choose one from many',
    checkbox: 'Choose many from many',
  },
  jwtSecret: 'secret-fitsafe3453',
  mailgun: {
    apiKey: 'key-75850806b27bc6a119d2c6c2b474def5',
  },
  domain: 'mg.ctpusdev.com',
  email: {
    userNotification: {
      from: '<donotreply@fitsafe.com>',
    },
  },
  clientHost: 'http://fitsafe-web.ctpusdev.com',
};
