import _ from 'lodash';
import local from './local';
import global from './global';

// Todo: separate server and client configs or move config to root directory
export default _.merge({}, global, local);
