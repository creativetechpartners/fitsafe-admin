import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import language from 'containers/LanguageProvider/reducer';
import categories from 'containers/Categories/reducer';
import surveys from 'containers/Surveys/reducer';
import login from 'containers/Login/reducer';

export default function createReducer() {
  return combineReducers({
    login,
    routing: routerReducer,
    form: formReducer,
    language,
    categories,
    surveys,
  });
}
