import { fork } from 'redux-saga/effects';
import categoriesSaga from 'containers/Categories/sagas';
import surveysSaga from 'containers/Surveys/sagas';
import loginSaga from 'containers/Login/saga';

export default function* rootSaga() {
  return yield [
    fork(categoriesSaga),
    fork(surveysSaga),
    fork(loginSaga),
  ];
}
