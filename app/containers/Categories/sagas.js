import { takeEvery, put } from 'redux-saga/effects';
import api from 'utils/api';
import { CREATE_CATEGORY, LOAD_CATEGORIES, UPDATE_CATEGORY, REMOVE_CATEGORY } from './constants';
import {
  categoryCreated,
  categoriesLoaded,
  categoryCreateFail,
  categoryUpdated,
  categoryUpdateFail,
  categoryRemoved,
} from './actions';

function* loadCategories() {
  const { categories } = yield api.get('category');
  yield put(categoriesLoaded(categories));
}

function* createCategory({ data }) {
  try {
    const { category } = yield api.post('category', {
      body: data,
    });
    yield put(categoryCreated(category));
  } catch (err) {
    if (err.status === 400) {
      const { errors } = yield err.json();
      yield put(categoryCreateFail(errors));
    } else {
      throw err;
    }
  }
}

function* updateCategory({ data, id }) {
  try {
    const { category } = yield api.put(`category/${id}`, {
      body: data,
    });
    yield put(categoryUpdated(category));
  } catch (err) {
    if (err.status === 400) {
      const { errors } = yield err.json();
      yield put(categoryUpdateFail(errors));
    } else {
      throw err;
    }
  }
}

function* removeCategory({ id: categoryId }) {
  try {
    const { id } = yield api.delete(`category/${categoryId}`);
    yield put(categoryRemoved(id));
  } catch (err) {
    console.log(err);
  }
}

export default function* mainSaga() {
  yield* [
    takeEvery(LOAD_CATEGORIES, loadCategories),
    takeEvery(CREATE_CATEGORY, createCategory),
    takeEvery(UPDATE_CATEGORY, updateCategory),
    takeEvery(REMOVE_CATEGORY, removeCategory),
  ];
}
