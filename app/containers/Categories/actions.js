import {
  LOAD_CATEGORIES,
  CATEGORIES_LOADED,
  CREATE_CATEGORY,
  CATEGORY_CREATED,
  CREATE_CATEGORY_FAIL,
  UPDATE_CATEGORY,
  CATEGORY_UPDATED,
  UPDATE_CATEGORY_FAIL,
  REMOVE_CATEGORY,
  CATEGORY_REMOVED,
} from './constants';

export const loadCategories = () => ({
  type: LOAD_CATEGORIES,
});

export const categoriesLoaded = categories => ({
  type: CATEGORIES_LOADED,
  categories,
});

export const createCategory = data => ({
  type: CREATE_CATEGORY,
  data,
});

export const categoryCreated = category => ({
  type: CATEGORY_CREATED,
  category,
});

export const categoryCreateFail = errors => ({
  type: CREATE_CATEGORY_FAIL,
  errors,
});

export const updateCategory = (data, id) => ({
  type: UPDATE_CATEGORY,
  data,
  id,
});

export const categoryUpdateFail = errors => ({
  type: UPDATE_CATEGORY_FAIL,
  errors,
});

export const categoryUpdated = category => ({
  type: CATEGORY_UPDATED,
  category,
});

export const removeCategory = id => ({
  type: REMOVE_CATEGORY,
  id,
});

export const categoryRemoved = id => ({
  type: CATEGORY_REMOVED,
  id,
});
