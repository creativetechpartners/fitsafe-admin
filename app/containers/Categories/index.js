import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object, array } from 'prop-types';
import { push } from 'react-router-redux';
import CategoryForm from 'components/CategoryForm';
import CategoryCard from 'components/CategoryCard';
import AddButton from 'components/CreateFormButton';
import ConfirmDelete from 'components/ConfirmDelete';
import { loadCategories, createCategory, updateCategory, removeCategory } from './actions';
import './styles.scss';

class Categories extends Component {
  state = {
    categoryFormOpened: false,
    category: null,
    deletion: null,
  };

  componentWillMount() {
    this.props.loadCategories();
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.categoryValidationErrors.length) {
      this.setState({
        categoryFormOpened: false,
      });
    }
  }

  switchCreateCategory = () => {
    this.setState({
      categoryFormOpened: !this.state.categoryFormOpened,
      category: null,
    });
  };

  switchUpdateCategory = (event, category) => {
    event.stopPropagation();
    this.setState({
      categoryFormOpened: !this.state.categoryFormOpened,
      category,
    });
  };

  handleDelete = (id) => {
    this.props.removeCategory(id);
    this.setState({ deletion: null });
  };

  askForDelete = (e, id) => {
    e.stopPropagation();
    this.setState({ deletion: id });
  };

  render() {
    const { categories, goTo, categoryValidationErrors } = this.props;
    const categoriesKeys = Object.keys(categories);
    const {
      switchCreateCategory,
      switchUpdateCategory,
      askForDelete,
      handleDelete,
      state: { categoryFormOpened, deletion },
    } = this;
    return (
      <div className="categories-page">
        <ConfirmDelete
          show={!!deletion}
          onHide={() => this.setState({ deletion: null })}
          onDelete={() => handleDelete(deletion)}
        />
        {!categoriesKeys.length
          ? <div className="help-block">Start with create category</div>
          : <div>
            <h1>Categories</h1>
            <div className="categories-cards-container">
              {categoriesKeys.map(key =>
                  (<CategoryCard
                    key={key}
                    category={categories[key]}
                    clickHandler={() => goTo(`categories/${key}`)}
                    openUpdate={switchUpdateCategory}
                    deleteCategory={askForDelete}
                  />),
                )}
            </div>
          </div>}

        <CategoryForm
          category={this.state.category}
          show={categoryFormOpened}
          onHide={switchCreateCategory}
          onSubmit={this.props.createCategory}
          onUpdate={this.props.updateCategory}
          categoryValidationErrors={categoryValidationErrors}
        />
        <AddButton clickHandler={switchCreateCategory} />
      </div>
    );
  }
}

Categories.propTypes = {
  goTo: func.isRequired,
  loadCategories: func.isRequired,
  createCategory: func.isRequired,
  updateCategory: func.isRequired,
  removeCategory: func.isRequired,
  categories: object.isRequired,
  categoryValidationErrors: array.isRequired,
};

export default connect(
  ({ categories: { categories, categoryValidationErrors } }) => ({
    categories,
    categoryValidationErrors,
  }),
  dispatch =>
    bindActionCreators(
      {
        goTo: push,
        loadCategories,
        createCategory,
        updateCategory,
        removeCategory,
      },
      dispatch,
    ),
)(Categories);
