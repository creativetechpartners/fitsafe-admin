import { combineReducers } from 'redux';
import { combineEvents } from 'utils/combineEvents';
import {
  CATEGORIES_LOADED,
  CATEGORY_CREATED,
  CREATE_CATEGORY_FAIL,
  UPDATE_CATEGORY_FAIL,
  CATEGORY_UPDATED,
  CATEGORY_REMOVED,
} from './constants';

const categories = combineEvents({
  [CATEGORY_CREATED]: (state, { category }) => ({
    ...state,
    [category._id]: category,
  }),
  [CATEGORIES_LOADED]: (state, action) => action.categories,
  [CATEGORY_UPDATED]: (state, { category }) => ({
    ...state,
    [category._id]: category,
  }),
  [CATEGORY_REMOVED]: (state, { id }) => {
    const _categories = state;
    delete _categories[id];
    return { ..._categories };
  },
}, {});

const categoryValidationErrors = combineEvents({
  [CREATE_CATEGORY_FAIL]: (state, { errors }) => errors,
  [UPDATE_CATEGORY_FAIL]: (state, { errors }) => errors,
  [CATEGORY_CREATED]: () => [],
  [CATEGORY_UPDATED]: () => [],
}, []);

export default combineReducers({
  categories,
  categoryValidationErrors,
});
