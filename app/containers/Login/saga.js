import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { LOGIN_ADMIN, LOG_OUT } from './constants';
import api, { setToken } from '../../utils/api';
import { restoreToken, authSuccess, loginFail, clearToken } from './actions';


function* login({ data }) {
  try {
    const { token } = yield api.post('login_admin', { body: data });
    window.localStorage.setItem('token', token);
    setToken(token);
    yield put(authSuccess(token));
    yield put(push('/'));
  } catch (err) {
    const { error } = yield err.json();
    yield put(loginFail(error));
  }
}

function* logOut() {
  window.localStorage.clear();
  yield put(clearToken());
  yield put(push('/login'));
}

export default function* loginSaga() {
  const token = window.localStorage.getItem('token');
  if (token) {
    setToken(token);
    yield put(restoreToken(token));
  }
  yield* [
    takeEvery(LOGIN_ADMIN, login),
    takeEvery(LOG_OUT, logOut),
  ];
}
