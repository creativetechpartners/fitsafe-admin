import { combineReducers } from 'redux';
import { combineEvents } from '../../utils/combineEvents';
import { LOGIN_ADMIN_FAILED, LOGIN_ADMIN_SUCCESS, RESTORE_TOKEN, CLEAR_TOKEN } from './constants';

const loginError = combineEvents({
  [LOGIN_ADMIN_SUCCESS]: () => ({}),
  [LOGIN_ADMIN_FAILED]: (state, { error }) => error,
}, {});

const token = combineEvents({
  [LOGIN_ADMIN_SUCCESS]: (state, action) => action.token,
  [RESTORE_TOKEN]: (state, action) => action.token,
  [CLEAR_TOKEN]: () => null,
}, null);

export default combineReducers({
  loginError,
  token,
});
