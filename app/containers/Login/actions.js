import { LOGIN_ADMIN, LOGIN_ADMIN_FAILED, LOGIN_ADMIN_SUCCESS, RESTORE_TOKEN, LOG_OUT, CLEAR_TOKEN } from './constants';

export const loginAdmin = data => ({
  type: LOGIN_ADMIN,
  data,
});

export const loginFail = error => ({
  type: LOGIN_ADMIN_FAILED,
  error,
});

export const authSuccess = token => ({
  type: LOGIN_ADMIN_SUCCESS,
  token,
});

export const restoreToken = token => ({
  type: RESTORE_TOKEN,
  token,
});

export const logOut = () => ({
  type: LOG_OUT,
});

export const clearToken = () => ({
  type: CLEAR_TOKEN,
});
