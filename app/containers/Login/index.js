import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { push } from 'react-router-redux';
import { func, string } from 'prop-types';
import Input from 'components/Input';
import { loginAdmin } from './actions';
import './style.scss';

const LoginForm = (props) => {
  const { handleSubmit } = props;
  return (
    <div className="form-wrapper-login">
      <form onSubmit={handleSubmit}>
        <div>
          <Field
            name="email"
            placeholder="Email"
            component={Input}
            type="text"
          />
          <Field
            name="password"
            placeholder="Password"
            component={Input}
            type="password"
          />
        </div>
        <div className="login-error">
          <span>{props.err}</span>
        </div>
        <button type="submit" className="btn btn-raised btn-info">Sign in</button>
      </form>
    </div>
  );
};

LoginForm.defaultProps = {
  err: undefined,
};

LoginForm.propTypes = {
  handleSubmit: func.isRequired,
  err: string,
};

const wrappedInForm = reduxForm({
  form: 'login',
  onSubmit: (data, dispatch) => dispatch(loginAdmin(data)),
})(LoginForm);

export default connect(
  ({ login: { loginError } }) => ({ err: loginError.message }),
  dispatch => bindActionCreators({
    push,
  }, dispatch),
)(wrappedInForm);
