import React, { Component } from 'react';
import Select from 'react-select';
import Radio from 'components/questions/Radio';
import YesNo from 'components/questions/YesNo';
import Checkbox from 'components/questions/Checkbox';
import config from 'config';
import { object, bool, func, array } from 'prop-types';
import './styles.scss';

const options = Object.keys(config.formTypes).map(value => ({
  value,
  label: config.formTypes[value],
}));

class Question extends Component {
  constructor(props) {
    super(props);
    const { show, ...rest } = this.props.data || {};
    const showOptions = show
      ? {
        showAfterQuestionId: show.questionId,
        showAfterCondition: show.condition,
        showAfter: true,
      }
      : {};
    const oldQuestionData = {
      ...rest,
      ...showOptions,
    };
    this.state = this.props.data
      ? oldQuestionData
      : {
        type: 'yes-no',
        title: '',
        _id: null,
        showAfterQuestionId: null,
        showAfterCondition: true,
        showAfter: false,
      };
  }

  collectData = () => this.fields.map(field => field.value); // eslint-disable-line

  handleChangeType = type => this.setState({ type: type.value });
  handleChangeTitle = e => this.setState({ title: e.target.value });

  ['yes-no'] = () => (
    <YesNo
      main={this.props.main}
      ref={(c) => {
        this.customField = c;
      }}
      options={this.state.options}
    />
  );
  radio = () => (
    <Radio
      ref={(c) => {
        this.customField = c;
      }}
      options={this.state.options}
    />
  );
  checkbox = () => (
    <Checkbox
      ref={(c) => {
        this.customField = c;
      }}
      options={this.state.options}
    />
  );

  filterEmptyFields = (opt) => {
    const res = {
      ...opt,
      variants: [],
      variantsScores: [],
      suggestions: [],
    };

    opt.variants.forEach((variant, i) => {
      if (!variant) return;
      res.variants.push(variant);
      res.variantsScores.push(parseInt(opt.variantsScores[i], 10));
      res.suggestions.push(opt.suggestions[i]);
    });
    res.suggestions = res.suggestions.filter(s => typeof s === 'string');
    return res;
  };

  handleChangeShowAfterQuestionId = question =>
    this.setState({ showAfterQuestionId: question.value });

  handleChangeAfterCondition = (e) => {
    const newState = { showAfterCondition: e.currentTarget.value === 'true' };
    if (e.currentTarget.value !== 'true') {
      newState.showAfterQuestionId = null;
    }
    this.setState(newState);
  };

  handleChangeShowOption = (e) => {
    const value = e.currentTarget.value;
    const newState = { showAfter: value === 'true' };
    if (value !== 'true') {
      newState.showAfterQuestionId = null;
    }
    this.setState(newState);
  };

  get value() {
    const { showAfterCondition, showAfterQuestionId, _id, type, title, showAfter } = this.state;

    const show =
      showAfter && typeof showAfterCondition === 'boolean' && showAfterQuestionId
        ? {
          condition: showAfterCondition,
          questionId: showAfterQuestionId,
        }
        : null;
    const options = this.filterEmptyFields(this.customField.value); // eslint-disable-line

    return { _id, type, title, show, options };
  }

  render() {
    const { main, handleDelete, surveyQuestions } = this.props;
    const { type, title, showAfterQuestionId, showAfterCondition, showAfter } = this.state;
    const yesNoQuestionsOptions = surveyQuestions
      .filter(q => q)
      .filter(q => q.type === 'yes-no')
      .filter(q => q._id !== this.state._id)
      .map(q => ({ value: q._id, label: q.title }));

    if (main) {
      return (
        <div className="main-question-container">
          <textarea value={title} onChange={this.handleChangeTitle} />
          {this[type]()}
        </div>
      );
    }

    return (
      <div className="question-container">
        <span onClick={handleDelete} className="remove-button pull-right">
          <i className="material-icons">close</i>
        </span>
        type:
        <Select
          options={options}
          onChange={this.handleChangeType}
          value={type}
          clearable={false}
          disabled={main}
        />
        question:
        <textarea value={title} onChange={this.handleChangeTitle} />
        {this[type]()}
        show after question answered:
        <input
          type="radio"
          value={false}
          checked={!showAfter}
          onChange={this.handleChangeShowOption}
        />OFF
        <input
          type="radio"
          value
          checked={showAfter === true}
          onChange={this.handleChangeShowOption}
        />ON
        {showAfter && (
          <div>
            answer:
            <input
              type="radio"
              value
              checked={showAfterCondition === true}
              onChange={this.handleChangeAfterCondition}
            />Yes
            <input
              type="radio"
              value={false}
              checked={showAfterCondition === false}
              onChange={this.handleChangeAfterCondition}
            />No
            <Select
              options={yesNoQuestionsOptions}
              onChange={this.handleChangeShowAfterQuestionId}
              value={showAfterQuestionId}
              clearable={false}
              disabled={!showAfterCondition}
            />
          </div>
        )}
      </div>
    );
  }
}

Question.defaultProps = {
  data: undefined,
  main: false,
  handleDelete: () => {},
  surveyQuestions: [],
};

Question.propTypes = {
  data: object,
  main: bool,
  handleDelete: func,
  surveyQuestions: array,
};

export default Question;
