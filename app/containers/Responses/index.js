import React from 'react';
import { string } from 'prop-types';
import './styles.scss';

const Responces = ({ id, category }) => (
  <div className="responses-container">
    <h1>{category} {id}</h1>

    {Array.from({ length: 20 }).map((_, key) => {
      return (<div className="response" key={key}>test response</div>);
    })}
  </div>
);

Responces.propTypes = {
  id: string.isRequired,
  category: string.isRequired,
};

export default Responces;
