import { combineEvents } from 'utils/combineEvents';
import {
  DEFAULT_LOCALE,
} from 'containers/Layout/constants';
import {
  CHANGE_LOCALE,
} from './constants';

const languageProviderReducer = combineEvents({
  [CHANGE_LOCALE]: (state, { locale }) => locale,
}, { locale: DEFAULT_LOCALE });

export default languageProviderReducer;
