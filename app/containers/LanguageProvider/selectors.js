import { createSelector } from 'reselect';

const selectLanguage = ({ language }) => language;

const makeSelectLocale = () => createSelector(
  selectLanguage,
  ({ locale }) => locale,
);

export {
  selectLanguage,
  makeSelectLocale,
};
