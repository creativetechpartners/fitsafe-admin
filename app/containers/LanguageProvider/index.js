import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { IntlProvider } from 'react-intl';
import { node, string, object } from 'prop-types';
import { makeSelectLocale } from './selectors';

const LanguageProvider = ({ locale, messages, children }) => (
  <IntlProvider locale={locale} key={locale} messages={messages[locale]}>
    {React.Children.only(children)}
  </IntlProvider>
);

LanguageProvider.propTypes = {
  children: node.isRequired,
  locale: string.isRequired,
  messages: object.isRequired,
};

const mapStateToProps = createSelector(
  makeSelectLocale(),
  locale => ({ locale }),
);

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LanguageProvider);
