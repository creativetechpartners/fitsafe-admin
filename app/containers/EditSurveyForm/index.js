import React, { Component } from 'react';
import { func, bool, object, array } from 'prop-types';
import CreateEditForm from 'components/CreateEditForm';
import { Button, Modal } from 'react-bootstrap';

class EditSurveyForm extends Component {
  onSubmit = () => {
    const { onSubmit } = this.props;
    onSubmit(this.form.value);
  }

  extractErrorMessage = (errors) => {
    const [field, ...rest] = errors[0].split(' ');
    const splittedField = field.split('.');
    let lastField = splittedField[splittedField.length - 1].replace(/[^a-zA-Z]+/g, '');
    if (lastField === 'title') {
      lastField = 'question';
    }

    if (lastField === 'name') {
      lastField = 'title';
    }

    return [lastField, ...rest].join(' ');
  }

  render() {
    const { show, onHide, survey, errors } = this.props;
    const formattedSurvey = survey && {
      ...survey,
      questions: Object.keys(survey.questions).map(key => survey.questions[key]),
    };

    return (
      <Modal show={show} onHide={(e => e && onHide())}>
        <Modal.Header closeButton>
          <Modal.Title>
            Edit survey
          </Modal.Title>
        </Modal.Header>

        <Modal.Body bsClass="modal-body clearfix">
          <CreateEditForm ref={(c) => { this.form = c; }} survey={formattedSurvey} />
          <Button onClick={this.onSubmit} bsStyle="info" className="btn-raised pull-right">
            Update
          </Button>
          <Button onClick={onHide} className="btn-raised pull-right">
            Cancel
          </Button>
          {errors.length ? <span className="error">{this.extractErrorMessage(errors)}</span> : null}
        </Modal.Body>
      </Modal>
    );
  }
}

EditSurveyForm.defaultProps = {
  survey: undefined,
};

EditSurveyForm.propTypes = {
  onSubmit: func.isRequired,
  show: bool.isRequired,
  onHide: func.isRequired,
  survey: object,
  errors: array.isRequired,
};

export default EditSurveyForm;
