import React from 'react';
import './styles.scss';

const NotFound = () => {
  return (<div className="notfound-page-container">
    <h1>404 page not found</h1>
  </div>);
};

export default NotFound;
