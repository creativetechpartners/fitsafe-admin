import React, { Component } from 'react';
import { func, bool, array } from 'prop-types';
import CreateEditForm from 'components/CreateEditForm';
import { Button, Modal } from 'react-bootstrap';

class CreateSurveyForm extends Component {
  onSubmit = () => {
    const { onSubmit } = this.props;
    onSubmit(this.form.value);
  }

  extractErrorMessage = (errors) => {
    const [field, ...rest] = errors[0].split(' ');
    const splittedField = field.split('.');
    let lastField = splittedField[splittedField.length - 1].replace(/[^a-zA-Z]+/g, '');
    if (lastField === 'title') {
      lastField = 'question';
    }

    if (lastField === 'name') {
      lastField = 'title';
    }

    return [lastField, ...rest].join(' ');
  }

  render() {
    const { show, onHide, errors } = this.props;

    return (
      <Modal show={show} onHide={(e => e && onHide())}>
        <Modal.Header closeButton>
          <Modal.Title>
            Create survey
          </Modal.Title>
        </Modal.Header>
        <Modal.Body bsClass="modal-body clearfix">
          <CreateEditForm ref={(c) => { this.form = c; }} />
          <Button onClick={this.onSubmit} bsStyle="info" className="btn-raised pull-right">
            Create
          </Button>
          <Button onClick={onHide} className="btn-raised pull-right">
            Cancel
          </Button>
          {errors.length ? <span className="error">{this.extractErrorMessage(errors)}</span> : null}
        </Modal.Body>
      </Modal>
    );
  }
}

CreateSurveyForm.propTypes = {
  onSubmit: func.isRequired,
  show: bool.isRequired,
  onHide: func.isRequired,
  errors: array.isRequired,
};

export default CreateSurveyForm;
