import {
  LOAD_SURVEYS,
  SURVEYS_LOADED,
  CREATE_SURVEY,
  SURVEY_CREATED,
  UPDATE_SURVEY,
  SURVEY_UPDATED,
  DELETE_SURVEY,
  SURVEY_DELETED,
  SURVEY_VALIDATION_FAILED,
} from './constants';

export const loadSurveys = category => ({
  type: LOAD_SURVEYS,
  category,
});

export const surveysLoaded = surveys => ({
  type: SURVEYS_LOADED,
  surveys,
});

export const createSurvey = survey => ({
  type: CREATE_SURVEY,
  survey,
});

export const surveyCreated = survey => ({
  type: SURVEY_CREATED,
  survey,
});

export const updateSurvey = (data, surveyId) => ({
  type: UPDATE_SURVEY,
  data,
  surveyId,
});

export const surveyUpdated = survey => ({
  type: SURVEY_UPDATED,
  survey,
});

export const deleteSurvey = surveyId => ({
  type: DELETE_SURVEY,
  surveyId,
});

export const surveyDeleted = surveyId => ({
  type: SURVEY_DELETED,
  surveyId,
});

export const surveyValidationFail = errors => ({
  errors,
  type: SURVEY_VALIDATION_FAILED,
});
