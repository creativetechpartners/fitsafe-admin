import { combineReducers } from 'redux';
import { combineEvents } from 'utils/combineEvents';
import { SURVEYS_LOADED, SURVEY_CREATED, SURVEY_UPDATED, SURVEY_DELETED, SURVEY_VALIDATION_FAILED } from './constants';

const addSurvey = (state, { survey }) => ({
  ...state,
  [survey._id]: survey,
});

const removeSurvey = (state, { surveyId }) => ({
  ...state,
  [surveyId]: null,
});

const surveys = combineEvents({
  [SURVEYS_LOADED]: (state, action) => action.surveys,
  [SURVEY_CREATED]: addSurvey,
  [SURVEY_UPDATED]: addSurvey,
  [SURVEY_DELETED]: removeSurvey,
}, {});

export const surveyValidationsErrors = combineEvents({
  [SURVEY_VALIDATION_FAILED]: (state, { errors }) => errors,
  [SURVEY_CREATED]: () => [],
  [SURVEY_UPDATED]: () => [],
}, []);

export default combineReducers({
  surveys,
  surveyValidationsErrors,
});
