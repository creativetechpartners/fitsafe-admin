import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from 'utils/api';
import { LOAD_SURVEYS, CREATE_SURVEY, UPDATE_SURVEY, DELETE_SURVEY } from './constants';
import { surveysLoaded, surveyCreated, surveyUpdated, surveyDeleted, surveyValidationFail } from './actions';

function* loadSurveys({ category }) {
  try {
    const { surveys } = yield api.get('survey', { query: { category } });
    yield put(surveysLoaded(surveys));
  } catch (err) {
    if (err.status === 404) {
      yield put(push('/*'));
    } else {
      throw err;
    }
  }
}

function* createSurvey({ survey }) {
  try {
    const { survey: newSurvey } = yield api.post('survey', { body: survey });
    yield put(surveyCreated(newSurvey));
  } catch (err) {
    if (err.status === 400) {
      const { errors } = yield err.json();
      yield put(surveyValidationFail(errors));
    } else {
      throw err;
    }
  }
}

function* updateSurvey({ data, surveyId }) {
  try {
    const { survey: updatedSurvey } = yield api.put(`survey/${surveyId}`, { body: data });
    yield put(surveyUpdated(updatedSurvey));
  } catch (err) {
    if (err.status === 400) {
      const { errors } = yield err.json();
      yield put(surveyValidationFail(errors));
    } else {
      throw err;
    }
  }
}

function* removeSurvey({ surveyId }) {
  yield api.delete(`survey/${surveyId}`);
  yield put(surveyDeleted(surveyId));
}

export default function* mainSaga() {
  yield* [
    takeEvery(LOAD_SURVEYS, loadSurveys),
    takeEvery(CREATE_SURVEY, createSurvey),
    takeEvery(UPDATE_SURVEY, updateSurvey),
    takeEvery(DELETE_SURVEY, removeSurvey),
  ];
}
