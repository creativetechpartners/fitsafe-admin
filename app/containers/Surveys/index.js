import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object, array } from 'prop-types';
import { push } from 'react-router-redux';
import AddButton from 'components/CreateFormButton';
import SideNav from 'components/SideNav';
import CreateSurveyForm from 'containers/CreateSurveyForm';
import EditSurveyForm from 'containers/EditSurveyForm';
import ConfirmDelete from 'components/ConfirmDelete';
import SurveyDetails from 'components/SurveyDetails';
import { loadSurveys, createSurvey, updateSurvey, deleteSurvey } from './actions';
import './styles.scss';

class Surveys extends Component {
  state = {
    showCreateModal: false,
    editSurvey: null,
    surveys: [],
    activeSurveyId: null,
  };

  componentWillMount = () => {
    const { params } = this.props;
    this.props.loadSurveys(params.categoryId);
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.surveyValidationsErrors.length) {
      this.setState({
        showCreateModal: false,
        editSurvey: null,
      });
    }
  }

  setActiveSurvey = ({ _id }) => this.setState({ activeSurveyId: _id });

  openCreateSurvey = () => {
    this.setState({
      showCreateModal: !this.state.showCreateModal,
    });
  };

  handleCreate = (formData) => {
    const payload = {
      ...formData,
      category: this.props.params.categoryId,
    };
    this.props.createSurvey(payload);
  };

  handleUpdate = (formData) => {
    const payload = {
      ...formData,
      category: this.props.params.categoryId,
    };
    const { editSurvey: { _id } } = this.state;
    this.props.updateSurvey(payload, _id);
  };

  openEditSurvey = (survey) => {
    this.setState({
      editSurvey: survey,
    });
  };

  closeEditSurvey = () => {
    this.setState({
      editSurvey: null,
    });
  };

  handleDelete = (id) => {
    this.props.deleteSurvey(id);
    this.setState({ deleting: null });
  };

  render() {
    const { goTo, surveys, params, surveyValidationsErrors } = this.props;
    const {
      openCreateSurvey,
      openEditSurvey,
      closeEditSurvey,
      handleCreate,
      handleUpdate,
      handleDelete,
      state: { showCreateModal, editSurvey, deleting, activeSurveyId },
    } = this;
    const filteredSurveys = Object.values(surveys).filter(survey => !!survey);
    return (
      <div className="surveys-page">
        {filteredSurveys.length ? (
          <SideNav
            goTo={goTo}
            surveys={filteredSurveys}
            category={params.categoryId}
            onEdit={openEditSurvey}
            onDelete={({ _id }) => this.setState({ deleting: _id })}
            onSelect={this.setActiveSurvey}
            activeSurvey={activeSurveyId}
          />
        ) : (
          <div className="help-block">Start with create survey</div>
        )}
        <CreateSurveyForm
          show={showCreateModal}
          onHide={openCreateSurvey}
          onSubmit={handleCreate}
          errors={surveyValidationsErrors}
        />
        <EditSurveyForm
          show={!!editSurvey}
          survey={editSurvey}
          onHide={closeEditSurvey}
          onSubmit={handleUpdate}
          errors={surveyValidationsErrors}
        />
        <ConfirmDelete
          show={!!deleting}
          onHide={() => this.setState({ deleting: null })}
          onDelete={() => handleDelete(deleting)}
        />
        {!!filteredSurveys.length && <SurveyDetails survey={surveys[activeSurveyId]} />}
        <AddButton clickHandler={openCreateSurvey} />
      </div>
    );
  }
}

Surveys.propTypes = {
  goTo: func.isRequired,
  params: object.isRequired,
  loadSurveys: func.isRequired,
  surveys: object.isRequired,
  createSurvey: func.isRequired,
  updateSurvey: func.isRequired,
  deleteSurvey: func.isRequired,
  surveyValidationsErrors: array.isRequired,
};

export default connect(
  ({ surveys: { surveys, surveyValidationsErrors } }) => ({ surveys, surveyValidationsErrors }),
  dispatch =>
    bindActionCreators(
      {
        goTo: push,
        loadSurveys,
        createSurvey,
        updateSurvey,
        deleteSurvey,
      },
      dispatch,
    ),
)(Surveys);
