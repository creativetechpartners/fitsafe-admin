import React from 'react';
import { node } from 'prop-types';
import NavBar from 'components/NavBar';
import './style.scss';

const Layout = props => (
  <div className="layout">
    <NavBar />
    {React.Children.toArray(props.children)}
  </div>
);

Layout.propTypes = {
  children: node.isRequired,
};

export default Layout;
