
# Configuring
local.js which is inside app/config folder is used for configuring application.
See global.js for creation the local configuration file.
```javascript
// typical local config
export default {
  hotLoader: {
    host: 'localhost',
    port: 3000,
  },
  host: 'localhost',
  port: 3001,
  apiHost: 'localhost:3000',
  clientHost: 'localhost:9000'
}
```

# Development
Package requires node version >= 7.6.0.
To get started install the [Yarn package manager](https://yarnpkg.com/lang/en/docs/install/) on your system or use npm.

Install all dependencies using:
```sh
$ yarn install
```
If you have mongo client on machine skip this step
else you can create docker container with mongo (docker-compose also needed). Just run following script
```sh
$ yarn run dev:db:up
```
For starting development server execute:
```sh
$ yarn start
```
Open localhost:3000 in your browser

# Code
Style rules are enforced through [ESLint](http://eslint.org) using the AirBnB preset.
To validate your code run:
```sh
$ yarn run lint:eslint
```
intl with react-intl packages is used for translation and localization.
All static text across the repository should be wrapped in [react-intl](https://github.com/yahoo/react-intl/wiki) FormattedMessage component.

