import fs from 'fs';

const acc = {};
fs.readdirSync(__dirname)
.filter(fileName => fileName !== 'index.js' && fileName.slice(-5) === '.html')
.map(fileName => fileName.substring(0, fileName.length - 5))
.forEach(fileName => (acc[fileName] = fs.readFileSync(`${__dirname}/${fileName}.html`).toString()));

export default acc;
