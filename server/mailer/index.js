import Handlebars from 'handlebars';
import mailgunCreator from 'mailgun-js';
import templates from './mailTemplates';
import controllers from './mailControllers';
import config from '../../app/config';

const {
  domain,
  mailgun: {
    apiKey,
  },
} = config;

const mailgun = mailgunCreator({ apiKey, domain });

export default async (mailName, data) => {
  const controller = controllers[mailName];

  if (!controller) {
    throw new Error(`Can't find ${mailName} mail controller`);
  }

  const { templateData, mailOptions } = controller(data);

  if (!mailOptions.text) {
    const template = templates[mailName];

    if (!template) {
      throw new Error(`Can't find ${mailName} mail template`);
    }

    const compiledTemplate = Handlebars.compile(template);
    mailOptions.html = compiledTemplate(templateData || {});
  }

  if (process.env.NO_MAILS) {
    console.log('Message sent (Silent mod): ', mailOptions.subject);
    return;
  }

  await new Promise((ok, reject) => mailgun.messages()
    .send(mailOptions, (err, body) => {
      if (err) {
        reject(err);
        return;
      }

      console.log('Message sent: ', body, mailOptions.subject);
      ok(body);
    }));
};
