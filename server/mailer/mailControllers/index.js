import fs from 'fs';

const acc = {};
fs.readdirSync(__dirname)
  .filter(fileName => fileName !== 'index.js' && fileName.slice(-3) === '.js')
  .map(fileName => fileName.substring(0, fileName.length - 3))
  .forEach(fileName => (acc[fileName] = require(`./${fileName}.js`).default));

export default acc;
