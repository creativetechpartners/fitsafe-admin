import config from '../../../app/config';

const {
  apiHost,
  domain,
  email: {
    userNotification,
  },
} = config;

export default ({ name, email, reportLink }) => {
  const mailOptions = {
    to: email,
    subject: 'New Report Generation',
  };
  const templateData = { name, domain, apiHost, reportLink };
  Object.assign(mailOptions, userNotification);
  return { templateData, mailOptions };
};
