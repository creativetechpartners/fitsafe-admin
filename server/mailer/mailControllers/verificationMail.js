import config from '../../../app/config';

const {
  clientHost,
  domain,
  email: {
    userNotification,
  },
} = config;

export default ({ name, email, token }) => {
  const mailOptions = {
    to: email,
    subject: 'Registration Confirmation',
  };
  const templateData = { name, domain, clientHost, token };
  Object.assign(mailOptions, userNotification);
  return { templateData, mailOptions };
};
