import Webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import Koa from 'koa';
import send from 'koa-send';
import serve from 'koa-static';
import BodyParser from 'koa-bodyparser';
import path from 'path';
import morgan from 'koa-morgan';
import cors from 'koa2-cors';
import { configurePublic, configurePrivate } from './controllers';
import WebpackConfig from '../internals/webpack/webpack.dev.babel';
import config from '../app/config';

const prod = process.env.NODE_ENV === 'production';
const staticFolder = prod ? 'build' : 'static';

const { hotLoader: { host: hotHost, port: hotPort }, host, port } = config;

const app = new Koa();

// app.use((ctx, next) => {
//   console.log('========================================================================');
//   console.log('origin', ctx.origin);
//   console.log('headers', JSON.stringify(ctx.headers, null, 2));
//   return next(ctx);
// });

app.use(morgan('dev'));
app.use(cors({ origin: '*' }));

app.use(
  BodyParser({
    enableTypes: ['json', 'form'],
  }),
);

app.use(configurePublic());
app.use(configurePrivate());

if (prod) {
  app.use(serve(path.resolve(__dirname, '../build')));
}
app.use(serve(path.resolve(__dirname, '../static')));

app.use(async (ctx) => {
  await send(ctx, `/${staticFolder}/index.html`);
});

app.listen(port, () => console.log(`Server listen on ${port}`));

if (process.env.NODE_ENV !== 'production') {
  const devServer = new WebpackDevServer(Webpack(WebpackConfig), {
    noInfo: true,
    hot: true,
    stats: { color: true },
    proxy: { '*': `http://${host}:${port}` },
    publicPath: WebpackConfig.output.publicPath,
  });

  devServer.listen(hotPort, (err) => {
    if (err) {
      console.error(err); // eslint-disable-line no-console
    } else {
      console.log(`Hot Loader serves on http://${hotHost}:${hotPort}`); // eslint-disable-line no-console
    }
  });
}
