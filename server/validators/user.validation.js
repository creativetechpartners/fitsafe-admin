import { string, object } from 'yup';

export const userRegisterSchema = object().shape({
  firstName: string().required('First name is empty!'),
  lastName: string().required('Last name is empty!'),
  email: string()
    .email('Invalid email format!')
    .required('Email is empty!'),
  password: string()
    .required('Password is empty!')
    .min(4, 'To short password!'),
  businessName: string().required('Legal name of business is empty'),
  businessAddress: string().required('Address of business is empty!'),
  insuranceCarrier: string().required('Insurance Carrier is empty!'),
  policyNumber: string().required('Policy Number is empty!'),
});

export const userResendEmailSchema = object().shape({
  email: string()
    .email('Invalid email format!')
    .required('Please, enter the email'),
});
