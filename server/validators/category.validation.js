import yup from 'yup';

const categorySchema = yup.object().shape({
  name: yup.string().required().max(100, 'Category name is too long'),
  description: yup.string().required(),
});

export default categorySchema;
