import { object, array, string, number, mixed } from 'yup';

const surveySchema = object().shape({
  name: string().required(),
  category: string().required(),
  surveyQuestion: object().shape({
    title: string().required(),
    type: string().required(),
    options: object().required(),
  }),
  questions: array()
    .of(
      object().shape({
        title: string().required(),
        type: string().required(),
        options: object().shape({
          variants: array()
            .of(string().required())
            .required(),
          variantsScores: array()
            .of(
              number()
                .min(0)
                .max(3)
                .required(),
            )
            .required(),
          suggestions: array().of(string()),
        }),
        show: mixed(),
      }),
    )
    .required(),
});

export default surveySchema;
