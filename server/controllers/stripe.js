import Router from 'koa-router';
import stripeCreator from 'stripe';
import dateFormat from 'dateformat';
import User from '../models/user';

const keySecret = 'sk_test_8VkiERRaEQeLwVmIXMexXanf';
const stripe = stripeCreator(keySecret);

const chargeHandler = async (ctx) => {
  try {
    const { stripeEmail, stripeToken } = ctx.request.body;

    const now = new Date();

    const planName = `${stripeEmail}_${dateFormat(now, 'dd-mm-yyyy-h:MM:ss')}`;
    const amount = 699;

    const plan = await stripe.plans.create({
      name: planName,
      id: planName,
      interval: 'month',
      currency: 'usd',
      amount,
      trial_period_days: 30,
    });

    const customer = await stripe.customers.create({
      email: stripeEmail,
      source: stripeToken,
    });

    const subscription = await stripe.subscriptions.create({
      customer: customer.id,
      items: [{ plan: planName }],
    });

    if (subscription.status === 'active') {
      const user = await User.findOne({ email: customer.email });
      user.isPaid = true;
      const newUser = await user.save();
      ctx.response.body = newUser;
    } else {
      throw new Error('Wrong subscription status', subscription.status);
    }
  } catch (err) {
    ctx.response.body = {
      error: { message: err.message },
    };
  }
};

export default function configureStripe() {
  const router = Router();
  const url = 'charge';
  router.post(url, chargeHandler);
  return router.routes();
}
