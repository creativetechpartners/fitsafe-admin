import Router from 'koa-router';
import { Category, Question } from '../models';
import validation from '../validators/category.validation';


const getTotalPossible = (questions) => {
  const fullScoringValue = questions.reduce((acc, question) => {
    const { variantsScores = [] } = question.options;
    if (question.type === 'checkbox') {
      acc[question._id] = variantsScores.reduce((a, b) => a + b, 0);
    } else {
      acc[question._id] = Math.max(...variantsScores);
    }

    return acc;
  }, {});
  return fullScoringValue;
};

const getCategories = async (ctx) => {
  const categories = await Category.find({}).lean();
  const categoryQuestions = await Promise.all(
    categories.map(category => Question.find({ category: category._id }).lean()),
  );
  ctx.response.body = {
    categories: categories.reduce(
      (byId, category, i) => ({
        ...byId,
        [category._id]: {
          ...category,
          questionsTotal: categoryQuestions[i].length,
          possiblePoints: getTotalPossible(categoryQuestions[i]),
        },
      }),
      {},
    ),
  };
};

const create = async (ctx) => {
  const { name, description } = ctx.request.body;
  try {
    await validation.validate({
      name,
      description,
    });
    ctx.response.body = {
      category: await Category.create({ name, description }),
    };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = {
      ...err,
    };
  }
};

const update = async (ctx) => {
  try {
    const { name: newName, description: newDescription } = ctx.request.body;
    const categoryId = ctx.params.id;
    await validation.validate({
      name: newName,
      description: newDescription,
    });
    const category = await Category.findOne({ _id: categoryId });
    category.name = newName;
    category.description = newDescription;
    await category.save();
    ctx.response.status = 200;
    ctx.response.body = {
      category,
    };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = {
      ...err,
    };
  }
};

const remove = async (ctx) => {
  try {
    const categoryId = ctx.params.id;
    const removedCategory = await Category.findByIdAndRemove({ _id: categoryId });
    ctx.response.status = 200;
    ctx.response.body = {
      id: removedCategory._id,
    };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { ...err };
  }
};

export default function configureCategory() {
  const router = Router();
  const url = 'category';
  router.get(url, getCategories);
  router.post(url, create);
  router.put(`${url}/:id`, update);
  router.delete(`${url}/:id`, remove);
  return router.routes();
}
