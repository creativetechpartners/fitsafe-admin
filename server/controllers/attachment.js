import Router from 'koa-router';
import asyncBusboy from 'async-busboy';
// import fs from 'fs';
import { Attachment } from '../models';
import { saveFile } from '../services/attachment.service';

const create = async (ctx) => {
  console.log('upload attachment');
  try {
    const { files } = await asyncBusboy(ctx.req);

    const { user } = ctx.state;
    const { question } = ctx.params;
    const promises = files.map(async (file) => {
      const saved = await saveFile(file, user._id);
      const attachment = await Attachment.create({
        ...saved,
        user: user._id,
        question,
      });
      return attachment;
    });
    const attachments = await Promise.all(promises);
    ctx.response.body = {
      attachments,
    };
  } catch (error) {
    ctx.status = 400;
    ctx.response.body = {
      error,
    };
  }
};

const remove = async (ctx) => {
  const { question } = ctx.params;
  const { user } = ctx.state;

  try {
    // const attachments = await Attachment.find({
    //   user: user._id,
    //   question,
    // });

    // attachments.forEach(({ path }) => {
    //   const fullPath = `${__dirname}/../../static/${path}`;
    //   if (fs.existsSync(fullPath)) fs.unlinkSync(fullPath);
    // });

    await Attachment.remove({
      user: user._id,
      question,
    });
  } catch (err) {
    console.log(err);
  }

  ctx.response.body = {
    question,
  };
};

export default function configureAttachment() {
  const router = Router();
  router.post('attachment/:question', create);
  router.delete('attachment/:question', remove);
  return router.routes();
}
