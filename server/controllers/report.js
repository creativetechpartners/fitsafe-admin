import Router from 'koa-router';
import { generateReport } from '../services/report.service';
import sendMail from '../mailer';
import config from '../../app/config';
import { User } from '../models';

const createReport = async (ctx) => {
  try {
    const { firstName, email } = ctx.state.user;
    const pdf = await generateReport(ctx.request.body, ctx.state.user);
    const link = `${config.apiHost}${pdf.file}`;

    const user = await User.findOne({ _id: ctx.state.user._id });
    user.reportLink = link;
    await user.save();
    await sendMail('reportMail', {
      name: firstName,
      email,
      reportLink: pdf.file,
    });
    ctx.response.body = {
      pdf,
      link,
    };
  } catch (err) {
    console.log(err);
    ctx.response.status = 400;
    ctx.response.body = {
      ...err,
    };
  }
};

export default function configureReport() {
  const router = Router();
  router.post('report', createReport);
  return router.routes();
}
