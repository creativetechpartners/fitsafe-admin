import Router from 'koa-router';
import { Survey, Question, Category, Report } from '../models';
import validation from '../validators/survey.validation';

const getSurveys = async (ctx) => {
  const { category } = ctx.request.query;
  try {
    const isExist = await Category.findOne({ _id: category }).lean();
    if (!isExist) {
      ctx.response.status = 404;
      return;
    }
  } catch (er) {
    ctx.response.status = 404;
    return;
  }
  const surveys = await Survey.find({ category }).lean();
  const questions = await Question.find({
    survey: {
      $in: surveys.map(survey => survey._id),
    },
  });

  const mainQuestions = await Question.find({
    _id: {
      $in: surveys.map(survey => survey.surveyQuestion),
    },
  });

  const mainQuestionsById = mainQuestions.reduce(
    (byId, question) => ({
      ...byId,
      [question._id]: question,
    }),
    {},
  );

  const questionBySurvey = questions.reduce((bySurvey, question) => {
    const { survey } = question;
    const surveyQuestions = bySurvey[survey] || {};
    return {
      ...bySurvey,
      [survey]: {
        ...surveyQuestions,
        [question._id]: question,
      },
    };
  }, {});

  ctx.response.body = {
    surveys: surveys.reduce(
      (byId, survey) => ({
        ...byId,
        [survey._id]: {
          ...survey,
          surveyQuestion: mainQuestionsById[survey.surveyQuestion],
          questions: questionBySurvey[survey._id],
        },
      }),
      {},
    ),
  };
};

const baseSurveyResponse = (survey, surveyQuestion, surveyQuestions) => ({
  ...survey.toObject(),
  surveyQuestion,
  questions: surveyQuestions.reduce(
    (byId, question) => ({
      ...byId,
      [question._id]: question.toObject(),
    }),
    {},
  ),
});

const create = async (ctx) => {
  try {
    const {
      questions,
      name,
      category,
      surveyQuestion: questionData,
      significant,
    } = ctx.request.body;

    await validation.validate({
      name,
      questions,
      surveyQuestion: questionData,
      category,
    });

    const surveyQuestion = await Question.create(questionData);
    const survey = await Survey.create({
      name,
      category,
      surveyQuestion: surveyQuestion._id,
      significant,
    });
    const createQuestionsPromises = questions.map(({ title, options, type }) =>
      Question.create({
        title,
        options: {
          ...options,
          variantsScores: options.variantsScores.map(score => +score),
          suggestions: options.suggestions,
        },
        survey: survey._id,
        type,
        category,
      }),
    );

    await createQuestionsPromises.reduce((a, b) => a.then(() => b), Promise.resolve());

    const surveyQuestions = await Question.find({ survey: survey._id });

    ctx.response.body = {
      survey: baseSurveyResponse(survey, surveyQuestion, surveyQuestions),
    };
  } catch (err) {
    ctx.status = 400;
    ctx.response.body = err;
  }
};

const filterQuestions = (questions, dbQuestions) => {
  const questionsToDelete = dbQuestions.filter(
    dbQ => !questions.find(q => q._id === dbQ._id.toString()),
  );
  const questionsToUpdate = [];
  const questionsToCreate = [];

  questions.forEach((q) => {
    if (q._id && dbQuestions.find(dbQ => dbQ._id.toString() === q._id)) {
      return questionsToUpdate.push(q);
    }

    return questionsToCreate.push(q);
  });

  return {
    questionsToDelete,
    questionsToUpdate,
    questionsToCreate,
  };
};

const deleteScoring = async (questionsToDelete) => {
  const reports = await Report.find({}).lean();
  const deletedQuestionsIds = questionsToDelete.map(que => que._id.toString());

  if (deletedQuestionsIds.length) {
    await Promise.all(
      reports.map((report) => {
        const scoring = report.scoring.scoring;

        const newScoring = Object.keys(scoring).reduce((acc, categoryId) => {
          const category = scoring[categoryId];

          acc[categoryId] = Object.keys(category).reduce((acc2, surveyId) => {
            const answers = category[surveyId].answers;
            acc2[surveyId] = {
              ...category[surveyId],
              answers: Object.keys(answers || {}).reduce((acc3, questionId) => {
                if (~deletedQuestionsIds.indexOf(questionId)) return acc3;
                acc3[questionId] = answers[questionId];
                return acc3;
              }, {}),
            };
            return acc2;
          }, {});

          return acc;
        }, {});
        report.scoring.scoring = newScoring;

        const categoriesScores = report.scoring.categoriesScores;
        if (categoriesScores) {
          const newCategoriesScores = Object.keys(categoriesScores).reduce((acc, categoryId) => {
            const category = categoriesScores[categoryId];

            acc[categoryId] = Object.keys(category).reduce((acc2, questionId) => {
              if (~deletedQuestionsIds.indexOf(questionId)) return acc2;
              acc2[questionId] = category[questionId];
              return acc2;
            }, {});

            return acc;
          }, {});
          report.scoring.categoriesScores = newCategoriesScores;
        }

        const { _id: reportId, ...rest } = report;

        return Report.update({ _id: reportId }, rest);
      }),
    );
  }
};

const update = async (ctx) => {
  try {
    const {
      questions,
      name,
      category,
      surveyQuestion: questionData,
      significant,
    } = ctx.request.body;

    await validation.validate({
      name,
      questions,
      surveyQuestion: questionData,
      category,
    });

    const { id } = ctx.params;
    await Survey.update({ _id: id }, { name, significant });
    const survey = await Survey.findOne({ _id: id });
    const dbquestions = await Question.find({ survey: survey._id });
    const { questionsToDelete, questionsToUpdate, questionsToCreate } = filterQuestions(
      questions,
      dbquestions,
    );
    const { _id: mainId, ...rest } = questionData;

    await Promise.all([
      Question.update({ _id: mainId }, rest),
      ...questionsToCreate.map(({ options, ...restProps }) =>
        Question.create({
          ...restProps,
          options: {
            ...options,
            variantsScores: options.variantsScores.map(score => +score),
          },
          survey: survey._id,
          category,
        }),
      ),
      ...questionsToUpdate.map(({ _id, ...restProps }) => Question.update({ _id }, restProps)),
      ...questionsToDelete.map(({ _id }) => Question.remove({ _id })),
    ]);

    const updatedQuestion = await Question.find({ survey: survey._id });
    const surveyQuestion = await Question.findOne({ _id: mainId });

    await deleteScoring([...questionsToDelete, ...questionsToUpdate]);

    ctx.response.body = {
      survey: baseSurveyResponse(survey, surveyQuestion, updatedQuestion),
    };
  } catch (err) {
    console.log(err);
    if (err.name === 'ValidationError') {
      const { errors } = err;
      ctx.status = 400;
      ctx.response.body = { errors };
    } else {
      ctx.status = 400;
      ctx.response.body = err.message;
    }
  }
};

const remove = async (ctx) => {
  const { id } = ctx.params;
  const survey = await Survey.findOne({ _id: id });
  const questionsToDelete = await Question.find({ survey: survey._id });
  await deleteScoring(questionsToDelete);

  await Question.remove({ _id: survey.surveyQuestion });
  await Question.remove({ survey: id });
  await Survey.remove({ _id: id });

  ctx.response.body = {
    survey,
  };
};

export default function configureSurvey() {
  const router = Router();
  const url = 'survey';
  router.get(url, getSurveys);
  router.post(url, create);
  router.put(`${url}/:id`, update);
  router.delete(`${url}/:id`, remove);
  return router.routes();
}
