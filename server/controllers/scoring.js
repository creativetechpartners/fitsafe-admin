import Router from 'koa-router';
import { Report } from '../models';

const saveScoring = async (ctx) => {
  const { scoring } = ctx.request.body;
  const { _id: userId } = ctx.state.user;
  const exist = await Report.findOne({ user: userId });
  let report = null;

  scoring.scoring = Object.keys(scoring.scoring).reduce((acc, key) => {
    if (Object.keys(scoring.scoring[key]).length) {
      acc[key] = scoring.scoring[key];
    }

    return acc;
  }, {});

  scoring.categoriesScores = Object.keys(scoring.categoriesScores).reduce((acc, key) => {
    if (Object.keys(scoring.categoriesScores[key]).length) {
      acc[key] = scoring.categoriesScores[key];
    }

    return acc;
  }, {});

  if (
    Object.keys(scoring).every((key) => {
      return !Object.keys(scoring[key]).length;
    })
  ) {
    await Report.remove({ user: userId });
    ctx.response.body = {
      report: null,
    };
    return;
  }

  if (exist) {
    await Report.update({ _id: exist._id }, { scoring });
    report = await Report.findOne({ _id: exist._id });
  } else {
    report = await Report.create({ scoring, user: userId });
  }
  ctx.response.body = {
    report,
  };
};

const getScoring = async (ctx) => {
  ctx.response.body = {
    report: await Report.findOne({
      user: ctx.state.user._id,
    }),
  };
};

export default function configureScoring() {
  const router = Router();
  const url = 'scoring';
  router.get(url, getScoring);
  router.post(url, saveScoring);
  return router.routes();
}
