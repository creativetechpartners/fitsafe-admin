import Router from 'koa-router';
import jwt from 'koa-jwt';
import { API_PREFIX } from '../constants';
import configureSurvey from './survey';
import configureAttachment from './attachment';
import configureCategory from './category';
import configureUser from './user';
import configureReport from './report';
import configureScoring from './scoring';
import configureStripe from './stripe';
import configureStripeWebhook from './stripe_webhook';
import config from '../../app/config';

export function configurePrivate() {
  const privateRouter = Router({
    prefix: API_PREFIX,
  });
  privateRouter.use(jwt({ secret: config.jwtSecret }));
  privateRouter.use(configureSurvey());
  privateRouter.use(configureCategory());
  privateRouter.use(configureAttachment());
  privateRouter.use(configureReport());
  privateRouter.use(configureScoring());
  privateRouter.use(configureStripe());
  return privateRouter.routes();
}

export function configurePublic() {
  const publicRouter = Router({
    prefix: API_PREFIX,
  });
  publicRouter.use(configureUser());
  publicRouter.use(configureStripeWebhook());

  return publicRouter.routes();
}
