import Router from 'koa-router';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import User from '../models/user';
import { userRegisterSchema, userResendEmailSchema } from '../validators/user.validation';
import config from '../../app/config';
import { ConfirmationToken } from '../models';
import sendMail from '../mailer';
import { ACTIVE, IN_ACTIVE } from '../constants';

const getEmailRegexp = (email) => new RegExp(`^${email}$`, 'i');

const createUser = async (ctx) => {
  try {
    const saltRounds = 10;
    const {
      firstName,
      lastName,
      password,
      businessName,
      businessAddress,
      insuranceCarrier,
      policyNumber,
      isPaid,
      lastTransactions,
    } = ctx.request.body;

    let { email } = ctx.request.body;

    await userRegisterSchema.validate(ctx.request.body, { abortEarly: false });

    email = email.toLowerCase();

    const isExist = await User.findOne({ email: getEmailRegexp(email) });

    if (!isExist) {
      const hash = await bcrypt.hash(password, saltRounds);
      const user = await User.create({
        firstName,
        lastName,
        email,
        password: hash,
        status: IN_ACTIVE,
        businessName,
        businessAddress,
        insuranceCarrier,
        policyNumber,
        reportLink: '',
        isPaid: false,
        lastTransactions: null,
      });

      const token = await ConfirmationToken.create({ userId: user._id, status: ACTIVE });

      await sendMail('verificationMail', {
        name: firstName,
        domain: config.domain,
        host: config.host,
        token: token._id,
        email: user.email,
      });

      ctx.response.body = {
        email: user.email,
      };
    } else {
      ctx.status = 400;
      ctx.response.body = {
        error: { email: 'This email is already used!' },
      };
    }
  } catch (err) {
    let error = err;
    if (err.name === 'ValidationError') {
      error = err.inner.reduce((errors, currentError) => {
        return { ...errors, [currentError.path]: currentError.message };
      }, {});
    }
    ctx.status = 400;
    ctx.response.body = {
      error,
    };
  }
};

const userActivation = async (ctx) => {
  try {
    const { token: tokenId } = ctx.params;

    let token = null;
    let user = null;

    try {
      token = await ConfirmationToken.findOne({ _id: tokenId });
      user = await User.findOne({ _id: token.userId });
    } catch (err) {
      throw new Error('Invalid token');
    }

    if (!token || !user) {
      throw new Error('Invalid token');
    }

    if (token.status === IN_ACTIVE && user.status === ACTIVE) {
      ctx.status = 200;
      ctx.response.body = {
        success: true,
      };
      return;
    }

    if (token.status === ACTIVE && user.status === IN_ACTIVE) {
      token.status = IN_ACTIVE;
      await token.save();

      user.status = ACTIVE;
      await user.save();

      ctx.status = 200;
      ctx.response.body = {
        success: true,
      };
      return;
    }

    // if nothing matched
    throw new Error('Invalid token');
  } catch (err) {
    ctx.status = 400;
    ctx.response.body = {
      error: { message: err.message },
    };
  }
};

const login = async (ctx) => {
  try {
    const { password } = ctx.request.body;
    let { email } = ctx.request.body;

    email = email.toLowerCase();

    const user = await User.findOne({ email: getEmailRegexp(email) }).lean();
    const { status } = user;
    if (user && status === ACTIVE) {
      const compare = await bcrypt.compare(password, user.password);
      const { firstName, lastName, isPaid, lastTransactions, reportLink } = user;
      if (compare) {
        const signedUser = { firstName, lastName, email, isPaid, lastTransactions, reportLink };
        const token = await jwt.sign(user, config.jwtSecret);
        ctx.response.body = {
          user: signedUser,
          token,
        };
      } else {
        ctx.status = 400;
        ctx.response.body = {
          error: { message: 'Invalid credentials!' },
        };
      }
    } else if (status === IN_ACTIVE) {
      ctx.status = 400;
      ctx.response.body = {
        error: { message: 'You should confirm your email!' },
      };
    } else {
      ctx.status = 400;
      ctx.response.body = {
        error: { message: 'Invalid credentials!' },
      };
    }
  } catch (err) {
    ctx.status = 400;
    ctx.response.body = {
      error: { message: 'Invalid credentials!' },
    };
  }
};

const checkForResetPassword = async (ctx) => {
  try {
    let { email } = ctx.request.body;
    email = email.toLowerCase();

    const user = await User.findOne({ email });
    const { firstName } = user;
    if (user && user.status === ACTIVE) {
      const token = await ConfirmationToken.create({ userId: user._id, status: ACTIVE });
      if (token && token.status === ACTIVE) {
        await sendMail('verificationMailForReset', {
          name: firstName,
          domain: config.domain,
          host: config.host,
          token: token._id,
          email,
        });

        ctx.response.status = 200;
        ctx.response.body = {
          message: 'Verification mail for reset password sent you!',
        };
      }
    } else {
      ctx.response.status = 400;
    }
  } catch (error) {
    ctx.response.body = {
      error: { message: 'The email address you entered is not valid or is not active.' },
    };
    ctx.response.status = 400;
  }
};

const letResetPassword = async (ctx) => {
  try {
    const { token: tokenId } = ctx.params;
    const token = await ConfirmationToken.findOne({ _id: tokenId });
    const user = await User.findOne({ _id: token.userId });
    const { email } = user;
    if (token && user && token.status === ACTIVE) {
      ctx.response.status = 200;
      ctx.response.body = {
        user: {
          email,
        },
      };
    } else {
      ctx.response.status = 400;
      ctx.response.body = {
        error: {
          status: true,
        },
      };
    }
  } catch (err) {
    ctx.response.status = 400;
  }
};

const resetPassword = async (ctx) => {
  try {
    const saltRounds = 10;
    const { newPassword } = ctx.request.body;
    const { token: tokenId } = ctx.params;
    const token = await ConfirmationToken.findOne({ _id: tokenId });
    const user = await User.findOne({ _id: token.userId });
    if (token && user && token.status === ACTIVE) {
      const password = await bcrypt.hash(newPassword, saltRounds);
      user.password = password;
      token.status = IN_ACTIVE;
      await user.save();
      await token.save();
      ctx.response.status = 200;
      ctx.response.body = {
        success: true,
      };
    } else {
      ctx.response.status = 400;
    }
    ctx.response.status = 200;
  } catch (err) {
    ctx.response.status = 400;
  }
};

const loginAdmin = async (ctx) => {
  try {
    const { password } = ctx.request.body;
    let { email } = ctx.request.body;
    email = email.toLowerCase();

    const admin = config.admin;
    const token = await jwt.sign(admin, config.jwtSecret);
    if (email === admin.email && password === admin.password) {
      ctx.response.status = 200;
      ctx.response.body = {
        token,
      };
    } else {
      ctx.response.status = 400;
      ctx.response.body = {
        error: { message: 'Invalid credentials!' },
      };
    }
  } catch (err) {
    ctx.status = 400;
    ctx.response.body = {
      error: { message: 'Invalid credentials!' },
    };
  }
};

const resendActivationEmail = async (ctx) => {
  try {
    let { email } = ctx.request.body;

    await userResendEmailSchema.validate(ctx.request.body, { abortEarly: false });

    email = email.toLowerCase();

    const user = await User.findOne({ email });

    if (user && user.status === IN_ACTIVE) {
      const token = await ConfirmationToken.findOne({ userId: user._id, status: ACTIVE });

      await sendMail('verificationMail', {
        name: user.firstName,
        domain: config.domain,
        host: config.host,
        token: token._id,
        email: user.email,
      });
    }

    ctx.response.status = 200;
    ctx.response.body = { success: true };
  } catch (err) {
    ctx.status = 400;
    ctx.response.body = err;
  }
};

const resetReportLink = async (ctx) => {
  try {
    const user = ctx.state.user && (await User.findOne({ _id: ctx.state.user._id }));
    if (user) {
      user.reportLink = '';
      await user.save();
    } else throw new Error('User not found');
    ctx.response.status = 200;
    ctx.response.body = { user };
  } catch (err) {
    ctx.status = 400;
    ctx.response.body = err;
  }
};

export default function configureUser() {
  const router = Router();
  const url = 'user';
  router.post(url, createUser);
  router.post(`${url}/login`, login);
  router.post(`${url}/resetReportLink`, resetReportLink);
  router.put(`${url}/activation/:token`, userActivation);
  router.post(`${url}/check/for_reset`, checkForResetPassword);
  router.get(`${url}/let_reset_password/:token`, letResetPassword);
  router.put(`${url}/reset_password/:token`, resetPassword);
  router.post(`${url}/resend_activation-email`, resendActivationEmail);
  router.post('login_admin', loginAdmin);
  return router.routes();
}
