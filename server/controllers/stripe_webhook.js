import Router from 'koa-router';
import stripeCreator from 'stripe';
import User from '../models/user';
import Transactions from '../models/transactions';

const keySecret = 'sk_test_8VkiERRaEQeLwVmIXMexXanf';
const stripe = stripeCreator(keySecret);

const webhookHandler = async (ctx) => {
  const { type, data: { object } } = ctx.request.body;

  if (type === 'invoice.payment_succeeded') {
    const customer = await stripe.customers.retrieve(object.customer);

    const user = await User.findOne({ email: customer.email });
    user.isPaid = true;
    user.lastTransactions = Date(object.date);
    await user.save();

    await Transactions.create({
      userId: user._id,
      details: JSON.stringify(object),
    });
  }

  if (type === 'customer.subscription.deleted') {
    const customer = await stripe.customers.retrieve(object.customer);

    const user = await User.findOne({ email: customer.email });
    user.isPaid = false;
    await user.save();
  }
};

export default function configureStripeWebhook() {
  const router = Router();
  const url = 'paymentwebhooks';
  router.post(url, webhookHandler);
  return router.routes();
}
