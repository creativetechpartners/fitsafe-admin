import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const { ObjectId, Mixed } = Schema.Types;

const questionSchema = new Schema({
  title: String,
  type: String,
  survey: ObjectId,
  category: ObjectId,
  options: Mixed,
  show: Mixed,
});

export default mongoose.model('question', questionSchema);
