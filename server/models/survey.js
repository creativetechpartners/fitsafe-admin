import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const { ObjectId, Boolean } = Schema.Types;

const surveySchema = new Schema({
  name: String,
  category: ObjectId,
  surveyQuestion: ObjectId,
  significant: Boolean,
});

export default mongoose.model('survey', surveySchema);
