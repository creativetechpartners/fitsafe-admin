import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const categorySchema = new Schema({
  name: String,
  description: String,
});

export default mongoose.model('category', categorySchema);
