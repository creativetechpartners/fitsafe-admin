import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const confirmationTokenSchema = new Schema({
  userId: String,
  status: String,
});

export default mongoose.model('confirmationToken', confirmationTokenSchema);
