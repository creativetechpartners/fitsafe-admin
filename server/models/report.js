import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const { ObjectId, Mixed } = Schema.Types;

const reportSchema = new Schema({
  status: String,
  user: ObjectId,
  scoring: Mixed,
});

export default mongoose.model('report', reportSchema);
