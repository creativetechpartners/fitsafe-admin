import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const { ObjectId } = Schema.Types;

const attachmentSchema = new Schema({
  name: String,
  type: String,
  path: String,
  user: ObjectId,
  question: ObjectId,
});

export default mongoose.model('attachment', attachmentSchema);
