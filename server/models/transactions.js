import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const transactionsSchema = new Schema({
  userId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  }],
  details: String,
});

export default mongoose.model('transactions', transactionsSchema);
