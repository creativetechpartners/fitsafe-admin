import mongoose from 'mongoose';
import Survey from './survey';
import Question from './question';
import Category from './category';
import User from './user';
import ConfirmationToken from './confirmationToken';
import Attachment from './attachment';
import Report from './report';

const connectString = `${process.env.NODE_ENV !== 'production' ? 'mongodb://localhost' : 'db'}/fit-safe`;
console.log(`Connect to ${connectString}`);
mongoose.connect(connectString);
mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:')); // eslint-disable-line no-console
db.once('open', () => console.log('connected to DB')); // eslint-disable-line no-console

export {
  Survey,
  Question,
  Category,
  User,
  ConfirmationToken,
  Attachment,
  Report,
};
