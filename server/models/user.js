import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const userSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  status: String,
  businessName: String,
  businessAddress: String,
  insuranceCarrier: String,
  policyNumber: String,
  isPaid: Boolean,
  reportLink: String,
  lastTransactions: Date,
});

export default mongoose.model('user', userSchema);
