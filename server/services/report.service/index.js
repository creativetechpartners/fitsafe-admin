import pdf from 'html-pdf';
import uniq from 'unique-string';
import path from 'path';
import mkdirp from 'mkdirp';
import fs from 'fs';
import _ from 'lodash';
import { Question, Survey } from '../../models';
import getReport from './components/Report';

const numbersCreator = () => {
  let numberCounter = 0;
  return (arr) => arr.map((el) => ({ ...el, number: ++numberCounter }));
};

export const generateReport = async (
  { scoring, categoriesScores, attachments, categories },
  user,
) => {
  const preparedQuestions = Object.keys(categoriesScores).reduce(
    (byQuestion, key) => ({
      ...byQuestion,
      ...categoriesScores[key],
    }),
    {},
  );
  const userId = user._id;
  const surveys = await Survey.find({}).lean();

  const byCategory = await Object.keys(categories).reduce(async (accPromise, categoryId) => {
    const acc = await accPromise;
    const questions = categoriesScores[categoryId] || {};
    const questionIds = Object.keys(questions);

    const categorySurveys = await Survey.find({ category: categoryId }).lean();

    const orderedQuestions = _.flatten(
      await Promise.all(
        categorySurveys.map((survey) => Question.find({ survey: survey._id }).lean()),
      ),
    );

    const filteredQuestions = orderedQuestions.filter(
      (q) => ~questionIds.indexOf(q._id.toString()),
    );
    acc[categoryId] = filteredQuestions;
    return acc;
  }, Promise.resolve({}));

  const bySeverity = Object.keys(preparedQuestions).reduce((bySev, questionId) => {
    const currentSeverity = preparedQuestions[questionId];
    return {
      ...bySev,
      [currentSeverity]: [...(bySev[currentSeverity] || []), questionId],
    };
  }, {});

  const zeroQuestions = await Question.find({
    _id: {
      $in: bySeverity[0],
    },
  }).lean();
  const oneQuestions = await Question.find({
    _id: {
      $in: bySeverity[1],
    },
  }).lean();
  const twoQuestions = await Question.find({
    _id: {
      $in: bySeverity[2],
    },
  }).lean();
  const threeQuestions = await Question.find({
    _id: {
      $in: bySeverity[3],
    },
  }).lean();

  const answers = Object.keys(scoring).reduce((answs, category) => {
    const categoryAnswers = Object.keys(scoring[category]).reduce(
      (acc, survey) => ({
        ...acc,
        ...scoring[category][survey].answers,
      }),
      {},
    );
    return {
      ...answs,
      ...categoryAnswers,
    };
  }, {});

  const basePath = `/reports/${userId}`;
  const reportsPath = path.resolve(__dirname, `../../../static${basePath}`);
  mkdirp.sync(reportsPath);
  const fileName = `/report${uniq()}.pdf`;
  const addNumbers = numbersCreator();
  const html = getReport({
    bySeverity: {
      3: addNumbers(threeQuestions),
      2: addNumbers(twoQuestions),
      1: addNumbers(oneQuestions),
      0: addNumbers(zeroQuestions),
    },
    answers,
    categoriesScores,
    scoring,
    attachments,
    categories,
    byCategory,
    surveys,
    preparedQuestions,
    user,
  });

  if (process.env.NODE_ENV !== 'production') {
    fs.writeFileSync('./static/report.debug.html', html);
  }

  return new Promise((ok, rej) => {
    pdf
      .create(html, {
        format: 'Letter',
        header: {
          height: '25mm',
        },
      })
      .toFile(`${reportsPath}${fileName}`, (err, res) => {
        if (err) rej(err);
        ok({ ...res, file: `${basePath}${fileName}` });
      });
  });
};
