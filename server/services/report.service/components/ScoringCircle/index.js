import React from 'react';
import { number } from 'prop-types';
import path from 'path';
import styles from './styles';

const getPercent = (score, maxScore) => {
  return maxScore ? 100 - ((Math.round(score) / maxScore) * 100) : 0;
};

const round = (percent) => {
  return Math.round(percent / 10) * 10;
};

const getImageUrl = (image) => {
  return `file://${path.resolve(__dirname, `../../../../../static/img/circles/${image}.png`)}`;
};

const ScoringCircle = ({ score, maxScore }) =>
  (<div style={styles.container}>
    <img style={styles.image} src={getImageUrl(round(getPercent(score, maxScore)))} alt="chart" />
    <div style={styles.percent}>
      {Math.round(getPercent(score, maxScore))}
    </div>
  </div>);

ScoringCircle.propTypes = {
  score: number.isRequired,
  maxScore: number.isRequired,
};

export default ScoringCircle;
