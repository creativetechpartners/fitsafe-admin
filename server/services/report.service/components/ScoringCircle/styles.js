export default {
  container: {
    width: '110px',
    height: '110px',
    display: '-webkit-flex',
    '-webkit-align-items': 'center',
    '-webkit-justify-content': 'center',
    position: 'relative',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  percent: {
    position: 'absolute',
    fontSize: '35px',
    display: '-webkit-flex',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    '-webkit-align-items': 'center',
    '-webkit-justify-content': 'center',
  },
};
