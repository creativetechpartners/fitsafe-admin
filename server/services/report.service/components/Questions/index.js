import React from 'react';
import { array, object, string } from 'prop-types';
import Question from '../Question';

const Questions = ({ questions, answers, categoryId, category }) => {
  if (!questions.length) return null;

  return (
    <div>
      <div
        style={{
          marginTop: '20px',
          fontWeight: '300',
          fontSize: '20px',
          textDecoration: 'underline',
        }}
      >
        {category.name}
      </div>
      {questions.map(question => (
        <Question
          key={question._id}
          question={question}
          answer={answers[question._id]}
          categoryId={categoryId}
        />
      ))}
    </div>
  );
};

Questions.propTypes = {
  questions: array.isRequired,
  answers: object.isRequired,
  categoryId: string.isRequired,
};

export default Questions;
