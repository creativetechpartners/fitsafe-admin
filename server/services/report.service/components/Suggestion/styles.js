export default {
  colorSeverity0: {
    color: '#6FBB00',
  },
  colorSeverity1: {
    color: '#2E57A6',
  },
  colorSeverity2: {
    color: '#FF7F00',
  },
  colorSeverity3: {
    color: '#BF0006',
  },

  content: {
    color: 'black',
  },
};
