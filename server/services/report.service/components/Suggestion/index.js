import React from 'react';
import { object, any } from 'prop-types';
import styles from './styles';

export const getSuggestion = (question, answer) => {
  const { type, options: { suggestions = [], variants } } = question;
  if (type === 'yes-no') {
    return answer === false ? suggestions[0] : '';
  } else if (type === 'radio') {
    return suggestions[variants.findIndex(variant => variant === answer)];
  }
  return answer.map(ans => suggestions[variants.findIndex(variant => variant === ans)]);
};

const Suggestion = ({ question, answer, severity, showTitle, category, survey }) => {
  const suggestion = getSuggestion(question, answer);

  if (!suggestion) return null;

  return (
    <div>
      {showTitle &&
        <span>{`${category.name} - ${survey.name}`}</span>
      }  
      <ul style={styles[`colorSeverity${severity}`]}>
        {Array.isArray(suggestion)
          ? suggestion.map(suggestionItem => (<li key={suggestionItem}>
              <p style={styles.content}>{suggestionItem}</p>
            </li>))
          : <li><p style={styles.content}>{suggestion}</p></li>
        }
      </ul>
    </div>
  );
};

Suggestion.propTypes = {
  question: object.isRequired,
  answer: any.isRequired,
  severity: any.isRequired,
};

export default Suggestion;
