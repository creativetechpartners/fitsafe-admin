import React from 'react';
import { object } from 'prop-types';
import Questions from '../Questions';

const Answers = ({ byCategory, answers, styles, categories }) => {
  return (
    <div>
      <h3 style={styles}>DETAILED SUMMARY</h3>
      {Object.keys(byCategory).map(categoryId => (
        <div>
          <Questions
            key={categoryId}
            questions={byCategory[categoryId]}
            answers={answers}
            categoryId={categoryId}
            category={categories[categoryId]}
          />
        </div>
      ))}
    </div>
  );
};

Answers.propTypes = {
  byCategory: object.isRequired,
  answers: object.isRequired,
  styles: object.isRequired,
};

export default Answers;
