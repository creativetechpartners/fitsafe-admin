export default {
  unsplit1: {
    pageBreakBefore: 'always',
    breakBefore: 'always',
  },
  unsplit2: {
    pageBreakBefore: 'always',
    breakBefore: 'always',
    // pageBreakAfter: 'always',
    // breakAfter: 'always',
    textAlign: 'center',
  },
  header: {
    textAlign: 'center',
    fontWeight: '100',
    borderBottom: '2px solid gray',
  },
  subHeader: {
    textAlign: 'center',
    marginBottom: '10px',
    fontWeight: 'bolder',
  },
};
