import React from 'react';
import { object } from 'prop-types';
import path from 'path';
import styles from './styles';

const Attachments = ({ attachments, byCategory }) => {
  const attachmentsKeys = Object.keys(attachments);
  const questions = Object.keys(byCategory).reduce((acc, key) => [...acc, ...byCategory[key]], []);

  if (!attachmentsKeys.length) {
    return null;
  }

  return (
    <div style={styles.unsplit1}>
      <h1 style={styles.header}>PHOTO DOCUMENTATION</h1>

      {questions.map((question, key) => {
        const questionAttachments = attachments[question._id];
        if (!questionAttachments || question.options.attachmentsDisabled) {
          return null;
        }
        return (
          <div key={key} style={styles.unsplit2}>
            <h2 style={styles.subHeader}>{question.title}</h2>
            {questionAttachments.map((attachment, i) => {
              return (
                <img
                  alt="attachment"
                  src={`file://${path.resolve(
                    __dirname,
                    `../../../../../static/${attachment.path}`,
                  )}`}
                  key={i}
                  style={{
                    margin: '10px',
                    maxWidth: '100%',
                    maxHeight: '100%',
                    display: 'inline-block',
                  }}
                />
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

Attachments.defaultProps = {
  attachments: {},
};

Attachments.propTypes = {
  attachments: object,
  byCategory: object.isRequired,
};

export default Attachments;
