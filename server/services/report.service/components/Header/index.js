import React from 'react';
import path from 'path';
import styles from './styles';

const Header = () => {
  return (
    <div id="pageHeader">
      <img
        src={`file://${path.resolve(__dirname, '../../../../../static/img/header-background.png')}`}
        alt="header background"
      />
      <img
        style={styles.logo}
        src={`file://${path.resolve(__dirname, '../../../../../static/img/logo.png')}`}
        alt="header background"
      />
    </div>
  );
};

export default Header;
