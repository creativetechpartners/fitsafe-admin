export default {
  unsplit: {
    pageBreakAfter: 'always',
    breakAfter: 'always',
  },
  suggestionSeverity0: {
    borderTop: '5px solid #6FBB00',
  },
  suggestionSeverity1: {
    borderTop: '5px solid #2E57A6',
  },
  suggestionSeverity2: {
    borderTop: '5px solid #FF7F00',
  },
  suggestionSeverity3: {
    borderTop: '5px solid #BF0006',
  },
  consideration: {
    fontFamily: 'Arial, Helvetica, sans-serif',
    fontWeight: '100',
    margin: '10px 0',
    fontSize: '15px',
  },
};
