import React from 'react';
import { object } from 'prop-types';
import _ from 'lodash';
import Suggestion, { getSuggestion } from '../Suggestion';
import styles from './styles';

const severityWords = ['QUARTER', 'TERTIARY', 'SECONDARY', 'PRIMARY'];

const severityToRender = [3, 2];
const highestSeverity = 3;

const Suggestions = ({ byCategory, answers, surveys, preparedQuestions, categories }) => {
  return (
    <div style={styles.unsplit}>
      {severityToRender.map((severity) => {
        const isHighestSev = severity === highestSeverity;
        if (
          !Object.keys(preparedQuestions).filter(
            qId => (isHighestSev
                ? preparedQuestions[qId] >= severity
                : preparedQuestions[qId] === severity),
          ).length
        ) {
          return null;
        }

        return (
          <div style={styles[`suggestionSeverity${severity}`]} key={severity}>
            <h2 style={styles.consideration}>
              {severityWords[severity]} CONSIDERATIONS TO IMPROVE RISK PROFILE
            </h2>
            {Object.keys(byCategory).map((categoryId) => {
              const categoryQuestions = byCategory[categoryId];
              const categorySavQuestions = categoryQuestions.filter(
                question => (isHighestSev
                  ? preparedQuestions[question._id] >= severity
                  : preparedQuestions[question._id] === severity),
              );
              let prevSurvey;

              return categorySavQuestions.map((question, i) => {
                const showTitle = question.survey.toString() !== prevSurvey;
                prevSurvey = question.survey.toString();

                return (
                  <Suggestion
                    key={i}
                    question={question}
                    answer={answers[question._id]}
                    severity={severity}
                    survey={
                      surveys.filter(
                        survey => survey._id.toString() === question.survey.toString(),
                      )[0]
                    }
                    category={categories[question.category]}
                    showTitle={showTitle}
                  />
                );
              });
            })}
          </div>
        );
      })}
    </div>
  );
};

Suggestions.propTypes = {
  byCategory: object.isRequired,
  answers: object.isRequired,
};

export default Suggestions;
