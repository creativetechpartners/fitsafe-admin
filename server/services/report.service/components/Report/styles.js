export default {
  container: {
    fontFamily: 'sans-serif',
    fontWeight: '100',
  },
  header: {
    textAlign: 'center',
    fontWeight: '100',
  },
  subHeader: {
    marginBottom: '10px',
    fontWeight: 'bolder',
  },
  bodyContainer: {
    padding: '0 25px',
  },
};
