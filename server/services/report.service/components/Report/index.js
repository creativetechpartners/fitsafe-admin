import React from 'react';
import ReactDomServer from 'react-dom/server';
import { object } from 'prop-types';
import Answers from '../Answers';
import Attachments from '../Attachments';
import Suggestions from '../Suggestions';
import Header from '../Header';
import User from '../User';
import Score from '../Score';
import styles from './styles';

const Report = ({
  user,
  bySeverity,
  answers,
  attachments,
  avrScore,
  fullScore,
  categories,
  categoriesScores,
  byCategory,
  surveys,
  preparedQuestions,
}) => {
  return (
    <html lang="en">
      <body>
        <div style={styles.container}>
          <Header />
          <div style={styles.bodyContainer}>
            <h1 style={styles.header}>RISK PROFILE SUMMARY</h1>
            <User user={user} />
            <Score
              avrScore={avrScore}
              fullScore={fullScore}
              categories={categories}
              categoriesScores={categoriesScores}
            />

            <h3 style={styles.subHeader}>EXECUTIVE SUMMARY</h3>
            <Suggestions
              byCategory={byCategory}
              answers={answers}
              preparedQuestions={preparedQuestions}
              surveys={surveys}
              categories={categories}
            />

            <Answers
              byCategory={byCategory}
              answers={answers}
              styles={styles.subHeader}
              categories={categories}
            />

            <Attachments byCategory={byCategory} attachments={attachments} />
          </div>
        </div>
      </body>
    </html>
  );
};

Report.propTypes = {
  bySeverity: object.isRequired,
  answers: object.isRequired,
  attachments: object.isRequired,
};

export default (props) => ReactDomServer.renderToStaticMarkup(<Report {...props} />);
