import React from 'react';
import path from 'path';
import ScoringCircle from '../ScoringCircle';
import styles from './styles';

const getPercent = ({ score, maxScore }) => {
  return maxScore ? 100 - Math.round(score) / maxScore * 100 : 0;
};

const round = (percent) => {
  return Math.round(percent / 10) * 10;
};

const getImageUrl = (image) => {
  return `file://${path.resolve(__dirname, `../../../../../static/img/lines/${image}.png`)}`;
};

const getCategoryScoring = ({ possiblePoints }, categoriesScore = {}) => {
  return Object.keys(categoriesScore).reduce(
    ({ score, maxScore }, questionId) => {
      return {
        score: score + categoriesScore[questionId],
        maxScore: maxScore + possiblePoints[questionId],
      };
    },
    { score: 0, maxScore: 0 },
  );
};

const getAvrScoring = (categories, categoriesScores) => {
  return Object.keys(categories).reduce(
    (acc, categoryId) => {
      const { score, maxScore } = getCategoryScoring(
        categories[categoryId],
        categoriesScores[categoryId],
      );

      return {
        score: acc.score + score,
        maxScore: acc.maxScore + maxScore,
      };
    },
    { score: 0, maxScore: 0 },
  );
};

const Score = ({ categories, categoriesScores }) => {
  return (
    <div>
      <h1 style={styles.header}>YOUR RISK AREAS</h1>
      <div style={styles.container}>
        <div style={styles.leftSubContainers}>
          <ul style={styles.list}>
            {Object.keys(categories).map((key) => {
              const percent = getPercent(getCategoryScoring(categories[key], categoriesScores[key]));

              if (
                !categoriesScores[key] ||
                !Object.keys(categoriesScores[key]).length
              ) {
                return null;
              }

              return (
                <li style={styles.category}>
                  <div style={styles.categoryName}>
                    {categories[key].name}
                  </div>
                  <img style={styles.line} src={getImageUrl(round(percent))} alt="chart" />
                </li>
              );
            })}
          </ul>
        </div>
        <ScoringCircle {...getAvrScoring(categories, categoriesScores)} />
      </div>
    </div>
  );
};

export default Score;
