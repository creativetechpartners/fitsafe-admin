export default {
  container: {
    display: '-webkit-flex',
    '-webkit-justify-content': 'space-between',
  },
  header: {
    fontSize: '15px',
    fontWeight: 400,
    borderBottom: '1px solid gray',
  },
  leftSubContainers: {
    width: '400px',
  },
  category: {
    display: '-webkit-flex',
    '-webkit-justify-content': 'space-between',
    marginBottom: '2px',
  },
  list: {
    '-webkit-padding-start': '0px',
    '-webkit-margin-before': '0px',
  },
  line: {
    width: '200px',
  },
};
