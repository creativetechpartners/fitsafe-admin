export default {
  questionSeverity0: {
    borderBottom: '5px solid #6FBB00',
  },
  questionSeverity1: {
    borderBottom: '5px solid #2E57A6',
  },
  questionSeverity2: {
    borderBottom: '5px solid #FF7F00',
  },
  questionSeverity3: {
    borderBottom: '5px solid #BF0006',
  },

  questionSeverityGray: {
    borderBottom: '5px solid gray',
  },

  colorSeverity0: {
    color: '#6FBB00',
  },
  colorSeverity1: {
    color: '#2E57A6',
  },
  colorSeverity2: {
    color: '#FF7F00',
  },
  colorSeverity3: {
    color: '#BF0006',
  },

  unsplit: {
    pageBreakInside: 'avoid',
  },
  content: {
    color: 'black',
    marginBottom: '8px',
    marginTop: '0px',
    fontSize: '12px',
  },
  title: {
    marginBottom: '5px',
    fontWeight: 'bolder',
  },
  list: {
    paddingLeft: '25px',
    margin: '0px',
  },
};
