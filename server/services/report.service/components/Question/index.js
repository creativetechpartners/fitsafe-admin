import React from 'react';
import { object, any, string } from 'prop-types';
import styles from './styles';

const getAnswer = (answer, type) => {
  if (type === 'yes-no') {
    return answer ? ['Yes'] : ['No'];
  } else if (type === 'radio') {
    return [answer];
  }
  return answer;
};

const Question = ({ question: { title, type, number }, answer, categoryId }) => {
  const extractedAnswers = getAnswer(answer, type);

  return (
    <div style={{ ...styles[`questionSeverityGray`], ...styles.unsplit }}>
      <div>
        <p style={styles.title}>
          Question: {number}
        </p>
        <ul style={{ ...styles.list }}>
          <li>
            <p style={styles.content}>
              {title}
            </p>
          </li>
        </ul>
      </div>
      <div>
        <p style={styles.title}>
          Answer{extractedAnswers.length > 1 && 's'}
        </p>
        <ul style={{ ...styles.list }}>
          {extractedAnswers.map(item =>
            (<li>
              <p style={styles.content}>
                {item}
              </p>
            </li>),
          )}
        </ul>
      </div>
    </div>
  );
};

Question.propTypes = {
  question: object.isRequired,
  answer: any.isRequired,
  categoryId: string.isRequired,
};

export default Question;
