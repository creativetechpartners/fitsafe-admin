export default {
  container: { marginBottom: '15px' },
  row: {
    paddingBottom: '5px',
    color: '#000',
    fontSize: '13px',
  },
};
