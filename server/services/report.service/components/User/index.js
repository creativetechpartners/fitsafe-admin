import React from 'react';
import styles from './styles';

const User = ({ user }) => {
  return (
    <div style={styles.container}>
      <div style={styles.row}>
        {user.firstName} {user.lastName} ({user.email})
      </div>
      <div style={styles.row}>
        {user.businessName} {user.businessAddress}
      </div>
      <div style={styles.row}>
        {user.insuranceCarrier} {user.policyNumber}
      </div>
    </div>
  );
};

export default User;
