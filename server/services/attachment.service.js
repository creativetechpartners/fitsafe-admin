import { createWriteStream } from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';
import uuidv4 from 'uuid/v4';

const config = {
  pathToImages: path.resolve(__dirname, '../../static/attachments/'),
};

export const saveFile = async (file, userId) => {
  const { pathToImages } = config;
  const filename = uuidv4();
  const filePath = path.resolve(pathToImages, userId, filename);
  mkdirp.sync(`${pathToImages}/${userId}`);
  const stream = createWriteStream(filePath);
  file.pipe(stream);
  return {
    name: filename,
    type: file.mimeType,
    path: `attachments/${userId}/${filename}`,
  };
};
