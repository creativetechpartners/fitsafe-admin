import { Category, Question, Survey } from '../models';

const categories = [
  {
    name: 'General',
    surveys: [
      {
        name: 'Parking Lot',
        surveyQuestion: {
          title: 'Does the facility have a parking lot ?',
        },
        questions: [
          {
            title: 'Some question',
            type: 'yes-no',
            options: {
              variants: ['yes', 'no'],
              variantsScores: [1, 0],
            },
          },
        ],
      },
    ],
  },
];


const migrate = async () => {
  const promises = categories.map(async (category) => {
    const { name: categoryName, surveys } = category;
    const { _id: categoryId } = await Category.create({ name: categoryName });
    await Promise.all(
      surveys.map(async (survey) => {
        const { name: surveyName, questions, surveyQuestion } = survey;
        const { _id: surveyQuestionId } = await Question.create({
          title: surveyQuestion.title,
          type: 'yes-no',
          options: {
            variants: [],
            variantsScores: [],
          },
        });
        const { _id: surveyId } = await Survey.create({
          name: surveyName,
          category: categoryId,
          surveyQuestion: surveyQuestionId,
        });
        await Promise.all(
          questions.map(async ({ title, type, options }) => {
            await Question.create({
              title,
              survey: surveyId,
              type,
              options,
            });
          }),
        );
      }),
    );
  });
  return Promise.all(promises);
};

migrate().then(() => console.log('migrated'));
